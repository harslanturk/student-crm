<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentVisitDetayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('student_visit_detay',function (Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('student_visit_id');
          $table->text('aciklama');
          $table->integer('status');
          $table->integer('durum');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_visit_detay');
    }
}
