<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanlamaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planlama',function (Blueprint $table)
        {
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('personel_id');
          $table->integer('ogrenci_id');
          $table->string('title');
          $table->string('sinif');
          $table->string('servis');
          $table->timestamp('start_time');
          $table->timestamp('end_time');
          $table->integer('status');
          $table->string('color');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planlama');
    }
}
