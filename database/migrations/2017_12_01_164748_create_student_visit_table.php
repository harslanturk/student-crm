<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentVisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('student_visit',function (Blueprint $table){
          $table->increments('id');
          $table->integer('status');
          $table->integer('ogrenci_id');
          $table->integer('gonderen_id');
          $table->integer('alici_id');
          $table->text('aciklama');
          $table->timestamp('tarih');
          $table->integer('gizli');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_visit');
    }
}
