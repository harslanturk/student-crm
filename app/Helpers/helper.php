<?php
/**
 * Created by PhpStorm.
 * User: Hasan
 * Date: 17.10.2016
 * Time: 14:35
 */
namespace App\Helpers;
use App\User;
use Session;
use App\Modules;
use Auth;
use App\UserDelegation;
use App\Notification;
use Mail;
use App\StudentVisitDetay;
use App\StudentVisitStatus;
use App\AuthGroup;
use App\Authorization;
use DB;
use Carbon\Carbon;
use App\ProgramList;
use App\Ogrenci;


class Helper
{
    public static function shout($url)
    {
        $url = explode('/', $url);
        $modul = '/'.$url['0'].'/'.$url['1'];

        $id=Modules::where('url',$modul)->select('name')->first()->name;
        $sess=Session::get($id );
        return $sess;
    }
    public static function sessionReload()
    {
        $del_id=Auth::user()->delegation_id;
        $delegation=UserDelegation::where('id',$del_id)->select('auth')->first();
        $json=json_decode($delegation->auth);


        foreach ($json as $key => $str) {

            $name = Modules::where('id', $key)->select('name')->first()->name;
            $read = substr($str, 0, 1);
            $add = substr($str, 1, 1);
            $update = substr($str, 2, 1);
            $delete = substr($str, 3, 1);

            $data = array(
                'r' => $read,
                'a' => $add,
                'u' => $update,
                'd' => $delete,
            );
            Session::put($name, $data);
        }
    }
    public static function MonthNameConverter($oldMonth)
    {
        $search  = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $replace = array('OCAK', 'ŞUBAT', 'MART', 'NİSAN', 'MAYIS', 'HAZİRAN', 'TEMMUZ', 'AĞUSTOS', 'EYLÜL', 'EKİM', 'KASIM'    , 'ARALIK');
        $newMonth=str_replace($search, $replace, $oldMonth);
        return $newMonth;
    }
    public static function DateConverter($date)
    {
        $newdate = date_format (date_create ($date), 'd-m-Y');
        return $newdate;
    }
    public static function DateTimeConverter($date)
    {
        $newdate = date_format (date_create ($date), 'd-m-Y H:i');
        return $newdate;
    }
    public static function notification($user_id, $target_id, $data, $type, $url, $status)
    {
        $user = Auth::user();
        if($target_id == $user->id)
        {

        }
        else{
            if($type==1)
            {
                //Yeni Konu ve İşlem Eklendiğinde
                $content = $data['ogrenci_name']." adlı öğrencinin \"". $data['konu'] ."\" konusu size atandı.";
                $notification = new Notification();
                $notification->user_id = $user_id;
                $notification->target_id = $target_id;
                $notification->type = $type;
                $notification->url = $url;
                $notification->status = $status;
                $notification->content = $content;
                $notification->save();
                $mailSend = array();
                $mailUser = User::where('id', $user_id)->first();
                $sendUser = User::where('id', $target_id)->first();
                $mailSend['content'] = $content;
                $mailSend['user'] = $mailUser;
                $mailSend['url'] = $url;
                $mailSend['konu'] = $data['konu'];

                Mail::send('admin.mail.index', ['mailSend' => $mailSend], function ($m) use ($data,$sendUser) {
                $m->from('posta@cozumlazim.com', 'Yeni Konu Atandı');

                $m->to($sendUser->email, "SefkatRHA")->subject($data['konu'].' Konusu Size Atandı!');
            });
            }
            elseif($type==2)
            {
                //Yeni Danışman Notu Eklendiğinde
                $content = "Yeni bir danışman notunuz bulunmaktadır.";
                $allUsers=User::where('delegation_id', "1")->get();
                /*echo "<pre>";
                print_r($allUsers);
                die();*/
                foreach ($allUsers as $allUser)
                {
                    $notification = new Notification();
                    $notification->user_id = $user_id;
                    $notification->target_id = $allUser->id;
                    $notification->type = $type;
                    $notification->url = $url;
                    $notification->status = $status;
                    $notification->content = $content;
                    $notification->save();

                    /*echo $allUser->email."<br>";
                    echo "-----".$allUser['email']."<br>";*/

                    /*$mailSend = array();
                    $mailUser = User::where('id', $user_id)->first();
                    //$sendUser = User::where('id', $allUser->id)->first();
                    $mailSend['content'] = $content;
                    $mailSend['user'] = $mailUser;
                    $mailSend['url'] = $url;
                    $mailSend['konu'] = "Danışman Notu";
                    Mail::send('admin.mail.index', ['mailSend' => $mailSend], function ($m) use ($mailSend) {
                    $m->from('posta@cozumlazim.com', 'Yeni Danışman Notu Eklendi');

                    $m->to($allUser['email'], "SefkatRHA")->subject('Yeni Danışman Notu Eklendi.');
                    });*/
                }
            }
            elseif($type==3)
            {
                //Duyuru Girildiğinde
                $content = "Yeni bir duyuru eklendi.";
                $users = User::where('status', 1)->get();
                foreach($users as $value)
                {
                    $notification = new Notification();
                    $notification->user_id = $user_id;
                    $notification->target_id = $value->id;
                    $notification->type = $type;
                    $notification->url = $url;
                    $notification->status = $status;
                    $notification->content = $content;
                    $notification->save();

                    $mailSend = array();
                    $mailUser = User::where('id', $user_id)->first();
                    $sendUser = User::where('id', $value->id)->first();
                    $mailSend['content'] = $content;
                    $mailSend['user'] = $mailUser;
                    $mailSend['url'] = $url;
                    $mailSend['konu'] = "Yeni Duyuru Eklendi";
                    Mail::send('admin.mail.index', ['data' => $data], function ($m) use ($data,$sendUser) {
                    $m->from('posta@cozumlazim.com', 'Yeni Duyuru Eklendi');

                    $m->to($sendUser->email, "SefkatRHA")->subject('Yeni Duyuru Eklendi.');
                    });
                }
            }
            elseif($type==4)
            {
                //Kupona Yorum Yapıldığında
            }
            elseif($type==5)
            {
                //Size atanan konuya girildiğinde
            }
        }
    }
    public static function readNotification($url, $type)
    {
        $target = Auth::user();
        if($type == 5)
        {
            //Konuya Girildiğinde
            $getNotif = Notification::where('url', $url)->where('target_id', $target->id)->where('type', 1)->where('status', 1)->get();
            foreach($getNotif as $value)
            {
                //$bakUser = User::where('id', $value->user_id)->first();
                $content = $target->name." Bildiriminize giriş yaptı; \"".$value->content."\"";
                $notification = new Notification();
                $notification->user_id = $value->target_id;
                $notification->target_id = $value->user_id;
                $notification->type = $type;
                $notification->url = $url;
                $notification->status = 1;
                $notification->content = $content;
                $notification->save();
            }

            Notification::where('url', $url)->where('target_id', $target->id)->update(['status' => '0']);
        }
        else
        {
            Notification::where('url', $url)->where('target_id', $target->id)->update(['status' => '0']);
        }
    }
    public static function getAllPersonel()
    {
        $personel=User::where('status', 1)->get();
        return $personel;
    }
    public static function getAllOgrenciIslemleri()
    {
        $ogrenciIslemleri = OgrenciIslemleri::all();
        return $ogrenciIslemleri;
    }
    public static function getAllModules()    {

        $modules=Modules::where('status','1')->where('parent_id','0')->orderBy('priority','asc')->get();
        return $modules;
    }
    public static function getAllParModules()
    {
        $par_modules=Modules::where('status','1')->where('parent_id','>','0')->orderBy('priority','asc')->get();
        return $par_modules;
    }
    public static function getAllSetting()
    {
        $allSetting= Setting::find(1)->first();
        return $allSetting;
    }
    public static function durumCek($id)
    {
      $durum = StudentVisitDetay::where('student_visit_id', $id)->orderBy('id','desc')->first();

      return $durum;
    }
    public static function durumTanimlamasi($id)
    {
      $durum = StudentVisitStatus::where('id', $id)->first();
      return $durum;

    }

    public static function getAuthParent($id)
    {
      $del_id = User::where('id',$id)->first();
      $auths = DB::table('authorization')
                ->join('emc_modules','emc_modules.id','=','authorization.modul_id')
                ->where('group_id',$del_id->delegation_id)
                ->where('parent_id',0)
                ->where('menu',1)
                ->orderBy('authorization.id','asc')
                ->select('authorization.*','emc_modules.*')
                ->get();

      return $auths;
    }
    public static function getAuthSubParent($id)
    {
      $del_id = User::where('id',$id)->first();
      $auths = DB::table('authorization')
                ->join('emc_modules','emc_modules.id','=','authorization.modul_id')
                ->where('group_id',$del_id->delegation_id)
                ->where('emc_modules.parent_id','>',0)
                ->orderBy('authorization.id','asc')
                ->select('authorization.*','emc_modules.*')
                ->get();

      return $auths;
    }

    public static function authControl($slug,$auth)
    {
      $del_id = User::where('id',Auth::user()->id)->first();
      $authorization = DB::table('authorization')
     ->join('emc_modules','emc_modules.id','=','authorization.modul_id')
     ->where('group_id',$del_id->delegation_id)
     ->where('emc_modules.slug',$slug)
     ->where($auth,1)
     ->select('authorization.*','emc_modules.name')
     ->get();

     if ($authorization) {
       return 1;
     }else {
       return 0;
     }
    }
    public static function dmYHi($tarih,$format)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->format($format);
        return $result;
    }
    public static function programEdit($sinif,$gun,$a_b,$grup_no,$grup_id)
    {
      $ogrencis = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();
      $plan = ProgramList::where('grup_no',$grup_no)
                        ->where('sinif_id',$sinif)
                        ->where('a_b',$a_b)
                        ->where('calisma_gun',$gun)
                        ->where('grup_id',$grup_id)
                        ->first();
      //                   $deger = '';
      // foreach($ogrencis as $ogrenci){
      // if($ogrenci->id == $plan->ogrenci_id){
      // $deger .= '<option value="'.$ogrenci->id.'" selected>'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
      // }
      // else{
      // $deger .= '<option value="'.$ogrenci->id.'">'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
      //     }
      // }
      return $plan;
    }
    public static function programFilter($sinif,$gun,$a_b,$grup_no,$grup_id,$servis_id)
    {
      $ogrencis = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();
      if ($servis_id) {
      $plan = ProgramList::where('grup_no',$grup_no)
                        ->where('sinif_id',$sinif)
                        ->where('a_b',$a_b)
                        ->where('calisma_gun',$gun)
                        ->where('grup_id',$grup_id)
                        ->where('servis_id',$servis_id)
                        ->first();
      }else {
      $plan = ProgramList::where('grup_no',$grup_no)
                        ->where('sinif_id',$sinif)
                        ->where('a_b',$a_b)
                        ->where('calisma_gun',$gun)
                        ->where('grup_id',$grup_id)
                        ->first();
      }
      //                   $deger = '';
      // foreach($ogrencis as $ogrenci){
      // if($ogrenci->id == $plan->ogrenci_id){
      // $deger .= '<option value="'.$ogrenci->id.'" selected>'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
      // }
      // else{
      // $deger .= '<option value="'.$ogrenci->id.'">'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
      //     }
      // // }
      // echo '<pre>';
      // print_r($plan);
      // die();
      return $plan;
    }

    public static function ServisYazdir($sinif,$gun,$servis,$grup_no,$grup_id)
    {
      $plan = DB::table('plan_list')
                ->join('ogrenci','ogrenci.id','=','plan_list.ogrenci_id')
                ->where('sinif_id',$sinif)
                ->where('calisma_gun',$gun)
                ->where('servis_id',$servis)
                ->where('grup_no',$grup_no)
                ->where('grup_id',$grup_id)
                ->first();

      return $plan;
    }

    public static function SinifYazdir($sinif,$gun,$grup_no,$grup_id)
    {
      $plan = DB::table('plan_list')
                ->join('ogrenci','ogrenci.id','=','plan_list.ogrenci_id')
                ->where('sinif_id','like','%'.$sinif.'%')
                ->where('calisma_gun',$gun)
                ->where('grup_no',$grup_no)
                ->where('grup_id',$grup_id)
                ->first();

      return $plan;
    }
}
