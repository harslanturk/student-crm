<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remind extends Model
{
    protected $table = "remind";
    protected $fillable = [
        'konu', 'ogrenci_id', 'remind_date', 'content', 'status','user_id'
    ];
}
