<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','yetki','delegation_id', 'tcno', 'cinsiyet', 'dogum_tarihi', 'dogum_yeri', 'il', 'ilce', 'adres', 'baba', 'anne', 'kayit_tarihi', 'ayrilis_tarihi', 'telefon', 'is_telefon', 'diger_telefon', 'status','color','sinif_id','servis_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function delegation()
    {
        return $this->belongsTo('App\UserDelegation');
    }
}
