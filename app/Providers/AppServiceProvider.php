<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Personel;
use App\User;
use App\OgrenciIslemleri;
use App\Modules;
use App\Setting;
use App\Notification;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $personel=User::where('status', 1)->orderBy('name', 'ASC')->get();
        $ogrenciIslemleri = OgrenciIslemleri::all();
        $modules=Modules::where('status','1')->where('parent_id','0')->orderBy('priority','asc')->get();
        $par_modules=Modules::where('status','1')->where('parent_id','>','0')->orderBy('priority','asc')->get();
        $allSetting= Setting::find(1)->first();


        view()->share('modules', $modules);
        view()->share('par_modules', $par_modules);
        view()->share('allSetting',$allSetting);
        view()->share('allpersonel', $personel);
        view()->share('allogrenciIslemleri', $ogrenciIslemleri);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
