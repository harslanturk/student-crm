<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanGroup extends Model
{
    protected $table = 'plan_group';
    protected $fillable = [
      'name','seans_one_start','seans_one_end','seans_two_start','seans_two_end','mola','status'
    ];
}
