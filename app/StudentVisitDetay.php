<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentVisitDetay extends Model
{
    protected $table = 'student_visit_detay';
    protected $fillable = [
      'user_id','student_visit_id','aciklama','status','durum'
    ];
}
