<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonelResim extends Model
{
    protected $table = "personel_resim";
    protected $fillable = [
        'personel_id', 'resim',
    ];
}
