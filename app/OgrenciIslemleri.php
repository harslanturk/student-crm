<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OgrenciIslemleri extends Model
{
    protected $table = "ogrenci_islemleri";
    protected $fillable = [
        'islem',
    ];
}
