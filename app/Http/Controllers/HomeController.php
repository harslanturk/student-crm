<?php

namespace App\Http\Controllers;

use App\EngelTipleri;
use App\Http\Requests;
use App\IslemDetay;
use App\IslemDetayList;
use App\Message;
use App\MessageList;
use App\Ogrenci;
use App\OgrenciIslemleri;
use App\OgrenciResim;
use App\Personel;
use App\User;
use App\Notice;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use App\Helpers\Helper;
use App\UserDelegation;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Helper::sessionReload();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personelCount = DB::table('users')->count();
        $ogrenciCount = DB::table('ogrenci')->count();
        $messageCount = DB::table('message')->count();
        $konuCount = DB::table('islem_detay')->where('alici_id', Auth::user()->id)->count();
        $notice = Notice::all();
        return view('admin.home',['ogrenciCount' => $ogrenciCount, 'personelCount' => $personelCount, 'messageCount' => $messageCount, 'konuCount' => $konuCount, 'notices' => $notice]);
    }
    public function test()
    {
          $a = "Ahmet vurduruyor.";
        $b = $a;
        echo $a."-------------------".$b;
        die();

        /*for ($i=0; $i < 5; $i++) {
            $dene['hasan'.$i] = $i;
        }
        echo "<pre>";
        print_r($dene);
        die();*/
        /*$year = date("Y");
        echo $year;
        /*$path = "hasan.docx";
        $path = "falan.txt";
        //$path = "dene.pdf";
        $parcala = explode('.', $path);

        echo "<pre>";
        print_r($parcala);
        die();*/

        //$files = File::allFiles("/img/upload/1508678745/");
        //$files = Storage::put('/img/upload/1508678745', $fileContents);
        //echo "<h1>ALOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO</h1>";
        // Building directory path.
        //$directory = storage_path('..\public\img\upload\1508678745');
        /*$directory = base_path()."/public/img/upload/1508678745";
        echo $directory."<br>";
        
        // Will return an array of files in the directory
        // Each array element is Symfony\Component\Finder\SplFileInfo object
        $files = File::files($directory);
        
        foreach ($files as $file) {
            echo "<pre>";
            print_r($file);
            echo "<img src=\"$file\">"."<br>";
        }
        echo "<pre>";
        print_r($files);
        die();
        /*$delegation=UserDelegation::where('id', '1')->select('auth')->first();
        $json=json_decode($delegation->auth);
        echo "<pre>";
        print_r($json);
        die();
        foreach ($json as $key => $str) {}*/
        /*
         * Bu Alan Öğrenci Eklerken Öğrencinin idsiyle dosya yaratacaktır.
        $galleryId="11";
        $path = public_path().'/uploads/'.$galleryId;

        File::makeDirectory($path, $mode = 0777, true, true);
        return view('test',['path' => $path]);
        
        $a = "X";

        for ($i=1; $i<=5 ; $i++) {

            for ($j=1; $j<=$i; $j++) {
                echo $a;
            }
            echo "<br>";
        }*/

    }
}
