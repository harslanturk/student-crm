<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Personel;
use App\User;
use App\Message;
use App\MessageList;
use App\Helpers\helper;

class SupportController extends Controller
{
    public function listMessage()
    {
        if (helper::authControl('destek','read')) {
          $user = Auth::user();

          $message=Message::where('alici_id', $user->id)->get();
          $alici=User::where('id', $user->id)->first();
          return view('admin.support.message_list', ['message' => $message, 'alici' => $alici]);
        }
        return redirect()->action('Admin\HomeController@index');
    }
    public function composeMessage()
    {
        $user=User::all();
        return view('admin.support.message_compose', ['user' => $user]);
    }
    public function sendMessage(Request $request)
    {
        $personel = Personel::all();
        $alici_id = $request->alici_id;
        $gonderen_id = $request->gonderen_id;
        $konu = $request->subject;
        $mesaj = $request->message;
        foreach ($alici_id as  $isim =>$value)
        {
            if($isim==0 && $value==0)
            {
                foreach($personel as $value2)
                {
                    $message= new Message();
                    $message->gonderen_id=$gonderen_id;
                    $message->alici_id=$value2->id;
                    $message->konu=$konu;
                    $message->mesaj=$mesaj;
                    $message->save();
                }
                break;
            }
            else
            {
                $message= new Message();
                $message->gonderen_id=$gonderen_id;
                $message->alici_id=$value;
                $message->konu=$konu;
                $message->mesaj=$mesaj;
                $message->save();
            }

        }
        $messagelist=Message::where('gonderen_id', $gonderen_id)->get();

        return view('admin.support.message_send', ['messagelist' => $messagelist]);
    }
    public function gidenMessage()
    {
        $user = Auth::user();
        $messagelist=Message::where('gonderen_id', $user->id)->get();
        return view('admin.support.message_send', ['message' => $messagelist]);
    }
    public function showMessage($id="1")
    {
        $message=Message::where('id', $id)->first();
        $messagelist = MessageList::where('message_id', $id)->get();
        $user=User::where('id', $message->gonderen_id)->first();
        $authuser= Auth::user();
        return view('admin.support.message_show', ['message' => $message, 'user' => $user, 'authuser' => $authuser, 'messagelist' => $messagelist]);
    }
    public function cevapMessage(Request $request)
    {
        $message=$request->all();
        MessageList::create($message);
        $message=Message::where('id', $request->message_id)->first();
        $messagelist = MessageList::where('message_id', $message->id)->get();
        $authuser= Auth::user();
        /*echo "<pre>";
        print_r($message);
        die();*/
        $user=User::where('id', $message->gonderen_id)->first();
        return redirect()->back();
    }
}
