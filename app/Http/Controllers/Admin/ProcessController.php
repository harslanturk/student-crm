<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\IslemDetayList;
use App\IslemDetay;
use App\Ogrenci;
use App\OgrenciIslemleri;
use App\User;
use App\Remind;
use DB;
use App\Helpers\Helper;
use App\OgrenciResim;

class ProcessController extends Controller
{
    public function detayIslem($id="1")
    {
        $islemDetay = DB::table('islem_detay')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->join('users', 'islem_detay.olusturan_id', '=', 'users.id')
            ->join('users as au', 'islem_detay.alici_id', '=', 'au.id')
            ->where('islem_detay.id', $id)
            ->orderBy('created_at', 'desc')
            ->select('islem_detay.*','users.name as sorumlu','au.name as alici', 'ogrenci_islemleri.islem as islem')
            ->first();
        $ogrenci = Ogrenci::where('id', $islemDetay->ogrenci_id)->first();
        $ogrenciResim = OgrenciResim::where('ogrenci_id', $islemDetay->ogrenci_id)->first();
        $personel = User::where('id',$islemDetay->gonderen_id)->first();
        $allPersonel = User::where('status', 1)->get();
        $islemDetayList = DB::table('islem_detay_list')
            ->join('islem_detay', 'islem_detay_list.konu_id', '=', 'islem_detay.id')
            ->join('users', 'islem_detay_list.alici_id', '=', 'users.id')
            ->join('users as m2', 'islem_detay_list.gonderen_id', '=', 'm2.id')
            ->where('islem_detay_list.konu_id', $id)
            ->orderBy('created_at', 'asc')
            ->select('islem_detay_list.*','users.name as alici' ,'m2.name as gonderen')
            ->get();
        $islemDetayListLast = DB::table('islem_detay_list')
            ->join('islem_detay', 'islem_detay_list.konu_id', '=', 'islem_detay.id')
            ->join('users', 'islem_detay_list.alici_id', '=', 'users.id')
            ->where('islem_detay_list.konu_id', $id)
            ->orderBy('islem_detay_list.created_at', 'desc')
            ->select('users.id as sorumlu')
            ->first();
        if($islemDetayListLast)
        {
            $sorumlu = $islemDetayListLast->sorumlu;
        }
        else
        {
            $me = Auth::user();
            $sorumlu = $me->id;
        }
        $smsIslemDetayList = DB::table('islem_detay_list')
            ->join('islem_detay', 'islem_detay_list.konu_id', '=', 'islem_detay.id')
            ->join('users', 'islem_detay_list.alici_id', '=', 'users.id')
            ->where('islem_detay_list.konu_id', $id)
            ->where('islem_detay_list.issend', 0)
            ->orderBy('created_at', 'asc')
            ->select('islem_detay_list.*','users.name as sorumlu')
            ->get();
        $remind = Remind::where('status', 1)->where('konu', $id)->first();
        Helper::readNotification("/admin/process/show/".$id, 5);
        /*echo "<pre>";
        print_r($islemDetayListLast);
        die();*/
        return view('admin.process.islem_detay',['ogrenci' => $ogrenci, 'personel' => $personel,'islemDetayList' => $islemDetayList, 'sorumlu' => $sorumlu, 'smsIslemDetayList' => $smsIslemDetayList, 'islemDetay' => $islemDetay, 'ogrenciResim' => $ogrenciResim, 'remind' => $remind, 'allPersonel' => $allPersonel]);
    }
    public function saveKonu(Request $request)
    {
        $gonderen_id = Auth::user();
        $deger = $request->all();
        /*echo "<pre>";
        print_r($deger['konu']);
        die();*/
        $kontKonuDetay = IslemDetay::where('konu', $deger['konu_id'])->where('ogrenci_id', $deger['ogrenci_id'])->where('status', 1)->count();
        if($kontKonuDetay == 0)
        {
            $islemdetay = new IslemDetay();
            $islemdetay->ogrenci_id = $deger['ogrenci_id'];
            $islemdetay->konu = $deger['konu_id'];
            $islemdetay->olusturan_id = $gonderen_id->id;
            $islemdetay->gonderen_id = $gonderen_id->id;
            $islemdetay->alici_id = $deger['alici_id'];
            $islemdetay->islem = $deger['islem'];
            $islemdetay->aciklama = $deger['aciklama'];
            $islemdetay->status = 1;
            $islemdetay->save();

            $islemdetayList = new IslemDetayList();
            $islemdetayList->ogrenci_id = $deger['ogrenci_id'];
            $islemdetayList->konu_id = $islemdetay->id;
            $islemdetayList->gonderen_id = $gonderen_id->id;
            $islemdetayList->alici_id = $deger['alici_id'];
            $islemdetayList->islem = $deger['islem'];
            $islemdetayList->aciklama = $deger['aciklama'];
            $islemdetayList->status = 1;
            $islemdetayList->save();
            //Notifikasyon Alanı
            $ogrenci = Ogrenci::where('id', $deger['ogrenci_id'])->first();
            $konu = OgrenciIslemleri::where('id', $deger['konu_id'])->first();
            $ogrenciName = $ogrenci->ad." ".$ogrenci->soyad;
            $data = array();
            $data['ogrenci_name'] = $ogrenciName;
            $data['konu'] = $konu->islem;
            Helper::notification($gonderen_id->id, $deger['alici_id'], $data, "1", "/admin/process/show/".$islemdetay->id, "1");
        }


        return redirect()->back();

    }
    public function saveIslem(Request $request)
    {
        $gonderen_id = Auth::user();
        $deger = $request->all();
        /*echo "<pre>";
        print_r($deger);
        die();*/
        if($deger['islem']=="Red")
        {
            $status = 3;
        }
        else if($deger['islem']=="Onaylandı")
        {
            $status = 2;
        }
        else
            $status = 1;
        $islem = IslemDetay::where('id', $deger['islem_detay'])->first();
        $islem->alici_id = $deger['alici_id'];
        $islem->status = $status;
        $islem->save();

        $islemdetay = new IslemDetayList();
        $islemdetay->ogrenci_id = $deger['ogrenci_id'];
        $islemdetay->konu_id = $deger['islem_detay'];
        $islemdetay->gonderen_id = $gonderen_id->id;
        $islemdetay->alici_id = $deger['alici_id'];
        $islemdetay->islem = $deger['islem'];
        $islemdetay->aciklama = $deger['aciklama'];
        $islemdetay->status = 1;
        $islemdetay->save();

        //Notifikasyon Alanı
        $ogrenci = Ogrenci::where('id', $deger['ogrenci_id'])->first();
        $konu_islem = IslemDetay::where('id', $deger['islem_detay'])->first();
        $konu = OgrenciIslemleri::where('id', $konu_islem->konu)->first();
        $ogrenciName = $ogrenci->ad." ".$ogrenci->soyad;
        $data = array();
        $data['ogrenci_name'] = $ogrenciName;
        $data['konu'] = $konu->islem;
        Helper::notification($gonderen_id->id, $deger['alici_id'], $data, "1", "/admin/process/show/".$konu_islem->id, "1");

        /*echo "<pre>";
        print_r($deger);
        die();*/
        return redirect()->back();

    }
    public function islemDetayOgrenci($id="1")
    {
        $islemDetay = IslemDetay::where('id', $id)->first();
        $ogrenci = Ogrenci::find($islemDetay->ogrenci_id)->first();
        return view('admin.process.islem_detay',['ogrenci' => $ogrenci, 'islemDetay' => $islemDetay]);
    }
    public function openKonu($id)
    {
        $islemDetay = IslemDetay::where('id', $id)->first();
        $islemDetay->status = 1;
        $islemDetay->save();
        return redirect()->back();
    }
    public function myWork()
    {
        $user = Auth::user();
        /*$islem_detay = DB::select("SELECT ogrenci.ad as ogrenci_ad, ogrenci.soyad as ogrenci_soyad, islem_detay_list.*, ogrenci_islemleri.islem as konu_islem, users.name as sorumlu FROM (SELECT * FROM islem_detay_list order by id DESC) AS islem_detay_list
            INNER JOIN ogrenci ON ogrenci.id = islem_detay_list.ogrenci_id
            INNER JOIN islem_detay ON islem_detay.id = islem_detay_list.konu_id
            INNER JOIN ogrenci_islemleri ON islem_detay.konu = ogrenci_islemleri.id
            INNER JOIN users ON users.id = islem_detay.alici_id
            WHERE islem_detay.alici_id = '$user->id'
            group by islem_detay_list.konu_id
            order by islem_detay_list.id ASC");*/
        /*$islem_detay = DB::table('islem_detay_list')
            ->join('islem_detay', 'islem_detay.id', '=', 'islem_detay_list.konu_id')
            ->where(function ($query) {
                $query->where('islem_detay_list.islem', "İşlem Yapılıyor")
                      ->orWhere('islem_detay_list.islem', "Yeni İşlem");
            })
            ->where('islem_detay_list.alici_id', $user->id)
            ->where('islem_detay.status', 1)
            ->orderBy('islem_detay_list.created_at', 'DESC')
            ->groupBy('islem_detay_list.konu_id')
            ->select('islem_detay_list.created_at','islem_detay_list.ogrenci_id','islem_detay_list.konu_id','islem_detay_list.gonderen_id','islem_detay_list.islem')
            ->get();*/
            $islem_detay = DB::select( DB::raw("SELECT t1.ogrenci_id, t1.konu_id, t1.gonderen_id, t1.alici_id, t1.islem, t1.aciklama, t1.created_at,ogrenci.ad ogrenci_ad, ogrenci.soyad ogrenci_soyad, ogrenci_islemleri.islem konu_islem, users.name sorumlu FROM islem_detay_list t1
JOIN (SELECT islem_detay_list.konu_id, MAX(islem_detay_list.created_at) created_at FROM islem_detay_list GROUP BY islem_detay_list.konu_id) t2
    ON t1.konu_id = t2.konu_id AND t1.created_at = t2.created_at
    JOIN islem_detay ON islem_detay.id = t1.konu_id
    JOIN ogrenci ON ogrenci.id = t1.ogrenci_id
    JOIN ogrenci_islemleri ON ogrenci_islemleri.id = islem_detay.konu
    JOIN users ON users.id = t1.gonderen_id
     WHERE t1.`status` != 0 AND (t1.islem = 'İşlem Yapılıyor' OR t1.islem = 'Yeni İşlem') AND t1.alici_id = '$user->id' AND t1.alici_id = '$user->id' ORDER BY t1.id DESC") );

     $delegation_id = User::where('id',$user->id)->first();
     $kullanicilar = User::all();

       /*echo "<pre>";
        print_r($islem_detay);
        die();*/
        return view('admin.process.mywork',['islemDetay' => $islem_detay,'delegation_id' => $delegation_id,'kullanicilar' => $kullanicilar]);
    }
    public function searchIslem(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        if (!empty($request->kullanici)) {
          $kullanici = $data['kullanici'];
        }else{
          $kullanici = $user->id;
        }
        $islem = $data['islem'];
        if($islem == "Onaylandı")
        {
            $status = "2";
        }
        else if($islem == "Red")
        {
            $status = "3";
        }
        else
        {
            $status = "1";
        }

        /*$islem_detay = DB::select("SELECT ogrenci.ad as ogrenci_ad, ogrenci.soyad as ogrenci_soyad, islem_detay_list.*, ogrenci_islemleri.islem as konu_islem, users.name as sorumlu FROM (SELECT * FROM islem_detay_list WHERE islem_detay_list.islem = '$islem' order by id DESC) AS islem_detay_list
            INNER JOIN ogrenci ON ogrenci.id = islem_detay_list.ogrenci_id
            INNER JOIN islem_detay ON islem_detay.id = islem_detay_list.konu_id
            INNER JOIN ogrenci_islemleri ON islem_detay.konu = ogrenci_islemleri.id
            INNER JOIN users ON users.id = islem_detay.alici_id
            WHERE islem_detay.alici_id = '$user->id'
            group by islem_detay_list.konu_id
            order by islem_detay_list.id ASC");*/
        /*$islem_detay = DB::table('islem_detay_list')
            ->join('islem_detay', 'islem_detay.id', '=', 'islem_detay_list.konu_id')
            ->where('islem_detay_list.islem', $islem)
            ->where('islem_detay_list.alici_id', $user->id)
            ->where('islem_detay.status', $status)
            ->orderBy('islem_detay_list.created_at', 'DESC')
            ->groupBy('islem_detay_list.konu_id')
            ->select('islem_detay_list.created_at','islem_detay_list.ogrenci_id','islem_detay_list.konu_id','islem_detay_list.gonderen_id','islem_detay_list.islem')
            ->get();*/
            $islem_detay = DB::select( DB::raw("SELECT t1.ogrenci_id, t1.konu_id, t1.gonderen_id, t1.alici_id, t1.islem, t1.aciklama, t1.created_at,ogrenci.ad ogrenci_ad, ogrenci.soyad ogrenci_soyad, ogrenci_islemleri.islem konu_islem, users.name sorumlu FROM islem_detay_list t1
JOIN (SELECT islem_detay_list.konu_id, MAX(islem_detay_list.created_at) created_at FROM islem_detay_list GROUP BY islem_detay_list.konu_id) t2
    ON t1.konu_id = t2.konu_id AND t1.created_at = t2.created_at
    JOIN islem_detay ON islem_detay.id = t1.konu_id
    JOIN ogrenci ON ogrenci.id = t1.ogrenci_id
    JOIN ogrenci_islemleri ON ogrenci_islemleri.id = islem_detay.konu
    JOIN users ON users.id = t1.gonderen_id
     WHERE islem_detay.`status` = '$status'  AND t1.alici_id = $kullanici ORDER BY t1.id DESC") );

     $delegation_id = User::where('id',$user->id)->first();
     $kullanicilar = User::all();
        /*echo "<pre>";
        print_r($islem_detay);
        echo $islem;
        die();*/
        return view('admin.process.mywork',['islemDetay' => $islem_detay,'delegation_id' => $delegation_id,'kullanicilar' => $kullanicilar]);
    }
    public function islemCek(Request $request)
    {
        $user= Auth::user();
        $data = $request->input('aliciId');
        $rv = '';
        if($data != 0)
        {
            if($data == $user->id)
            {
                $rv .= '<option>'."İşlem Yapılıyor".'</option>';
                $rv .= '<option>'."Onaylandı".'</option>';
                $rv .= '<option>'."Red".'</option>';
            }
            else
            {
                $rv .= '<option>'."İşlem Yapılıyor".'</option>';
            }
        }
        else
        {

        }
        return view('admin.process.cikti', ['rv' => $rv]);
    }
}
