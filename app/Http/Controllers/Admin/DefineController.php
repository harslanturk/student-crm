<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OgrenciIslemleri;
use App\EngelTipleri;
use App\StudentVisitStatus;
use App\Helpers\helper;
use App\Sinif;
use App\Servis;
use App\PlanGroup;

class DefineController extends Controller
{
    public function ogrenciIslemleri()
    {
        if (helper::authControl('tanimlamalar','read')) {
          $ogrenciIslemleri = OgrenciIslemleri::all();
          return view('admin.define.ogrenci_islemleri', ['ogrenciIslemleri' => $ogrenciIslemleri]);
        }
          return redirect()->action('Admin\HomeController@index');
    }
    public function engelTipleri()
    {
        $authorization = helper::authControl('tanimlamalar','read');
        if ($authorization) {
          $engelTipleri = EngelTipleri::all();
          return view('admin.define.engel_tipleri', ['engelTipleri' => $engelTipleri]);
        }
        return redirect()->action('Admin\HomeController@index');
    }
    public function studentVisitStatus()
    {
        $authorization = helper::authControl('tanimlamalar','read');
        if ($authorization) {
          $studentVisitStatus = StudentVisitStatus::all();
          return view('admin.define.student_visit_status', ['studentVisitStatus' => $studentVisitStatus]);
        }
          return redirect()->action('Admin\HomeController@index');
    }
    public function addOgrenciIslem(Request $request)
    {
        $islem=$request->islem;
        $ogrenciIslem = new OgrenciIslemleri();
        $ogrenciIslem->islem = $islem;
        $ogrenciIslem->save();
        return redirect()->back();
    }
    public function addEngelTipi(Request $request)
    {
        $engel=$request->engel;
        $engelTipi = new EngelTipleri();
        $engelTipi->engel = $engel;
        $engelTipi->save();
        return redirect()->back();
    }
    public function addStudentVisitStatus(Request $request)
    {
        $islem=$request->all();
        StudentVisitStatus::create($islem);
        return redirect()->back();
    }
    public function editEngelTipi(Request $request)
    {
        $engel_id = $request['engel_id'];

        $engel = EngelTipleri::where('id',$engel_id)->first();

        return view('admin.define.engel_tipleriModal',[
            'engel' => $engel
        ]);
    }

    public function saveEngelTipi(Request $request)
    {
        $data = $request->all();

        EngelTipleri::find($data['id'])->update($data);

        return redirect()->back();
    }

    public function editOgrenciIslem(Request $request)
    {
        $islem_id = $request['ogrenci_id'];

        $islem = OgrenciIslemleri::where('id',$islem_id)->first();

        return view('admin.define.ogrenci_islemModal',[
            'islem' => $islem
        ]);
    }

    public function saveOgrenciIslem(Request $request)
    {
        $data = $request->all();

        OgrenciIslemleri::find($data['id'])->update($data);

        return redirect()->back();
    }
    public function editStudentVisitStatus(Request $request)
    {
        $islem_id = $request['islem_id'];

        $islem = StudentVisitStatus::where('id',$islem_id)->first();

        return view('admin.define.student_visit_statusModal',[
            'islem' => $islem
        ]);
    }
    public function saveStudentVisitStatus(Request $request)
    {
        $data = $request->all();

        StudentVisitStatus::find($data['id'])->update($data);

        return redirect()->back();
    }

    public function sinifIndex()
    {
        $authorization = helper::authControl('tanimlamalar','read');
        if ($authorization) {
          $sinif = Sinif::where('status',1)->get();
          return view('admin.define.sinif', ['sinif' => $sinif]);
        }
        return redirect()->action('Admin\HomeController@index');
    }
    public function addSinif(Request $request)
    {
        $data=$request->all();
        $data['status'] = 1;
        Sinif::create($data);
        return redirect()->back();
    }
    public function editSinif(Request $request)
    {
        $sinif_id = $request['sinif_id'];

        $sinif = Sinif::where('id',$sinif_id)->first();

        return view('admin.define.sinifModal',[
            'sinif' => $sinif
        ]);
    }
    public function saveSinif(Request $request)
    {
        $data = $request->all();
        Sinif::find($data['id'])->update($data);

        return redirect()->back();
    }

    public function servisIndex()
    {
        $authorization = helper::authControl('tanimlamalar','read');
        if ($authorization) {
          $servis = Servis::where('status',1)->get();
          return view('admin.define.servis', ['servis' => $servis]);
        }
        return redirect()->action('Admin\HomeController@index');
    }
    public function addServis(Request $request)
    {
        $data=$request->all();
        $data['status'] = 1;
        Servis::create($data);
        return redirect()->back();
    }
    public function editServis(Request $request)
    {
        $servis_id = $request['servis_id'];

        $servis = Servis::where('id',$servis_id)->first();

        return view('admin.define.servisModal',[
            'servis' => $servis
        ]);
    }
    public function saveServis(Request $request)
    {
        $data = $request->all();
        Servis::find($data['id'])->update($data);

        return redirect()->back();
    }
    public function planGrupIndex()
    {
        if (helper::authControl('tanimlamalar','read')) {
          $planGroup = PlanGroup::all();
          return view('admin.define.plan_group', ['planGroup' => $planGroup]);
        }
          return redirect()->action('Admin\HomeController@index');
    }
    public function PlanGroupModal()
    {
      return view('admin.define.plan_groupModal');
    }
    public function addPlanGroup(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      PlanGroup::create($data);

      return redirect()->back();
    }
    public function planGrupEdit(Request $request)
    {
      $grup_id = $request['grup_id'];

      $plan = PlanGroup::where('id',$grup_id)->first();

      return view('admin.define.plan_groupEdit',[
          'plan' => $plan
      ]);
    }
    public function planGrupUpdate(Request $request)
    {
      $data = $request->all();
      PlanGroup::find($data['id'])->update($data);

      return redirect()->back();
    }
}
