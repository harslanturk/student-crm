<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Personel;
use App\Notification;
use Auth;
use App\UserDelegation;
use DB;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;
use App\PersonelResim;
use App\AuthGroup;
use App\Servis;
use App\Sinif;
use File;

class PersonelController extends Controller
{
    public function personelEkle()
    {
        // echo "<pre>";
        // print_r($authorization);
        // die();
        if (helper::authControl('personel-ekle','add')) {
          $userDelegation = AuthGroup::where('status',1)->get();
          $sinif = Sinif::where('status',1)->get();
          $servis = Servis::where('status',1)->get();
          return view('admin.personal.personel_ekle', [
            'userDelegation' => $userDelegation,
            'sinif' => $sinif,
            'servis' => $servis
          ]);
        }
        return redirect()->action('Admin\HomeController@index');

    }
    public function listPersonel()
    {
      //$personel = User::where();
      if (helper::authControl('tum-personeller','read')) {
        $personel = DB::table('users')
            ->join('auth_group', 'auth_group.id', '=', 'users.delegation_id')
            ->select('users.*', 'auth_group.name as delegName')
            ->orderBy('created_at','desc')
            ->where('users.status', 1)
            ->get();
        return view('admin.personal.personel_list',['personel' => $personel]);
      }
      return redirect()->action('Admin\HomeController@index');
    }
    public function addPersonel(Request $request)
    {
        $personel = $request->all();
        $name=$request->name." ".$request->surname;
        User::create([
            'name' => $name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'delegation_id' => $request->delegation_id,
            'tcno' => $request->tcno,
            'cinsiyet' => $request->cinsiyet,
            'dogum_tarihi' => $request->dogum_tarihi,
            'dogum_yeri' => $request->dogum_yeri,
            'baba' => $request->baba,
            'kayit_tarihi' => $request->kayit_tarihi,
            'anne' => $request->anne,
            'ayrilis_tarihi' => $request->ayrilis_tarihi,
            'telefon' => $request->telefon,
            'is_telefon' => $request->is_telefon,
            'diger_telefon' => $request->diger_telefon,
            'il' => $request->il,
            'ilce' => $request->ilce,
            'adres' => $request->adres,
            'status' => 1,
            'color' => $request->color,
            'sinif_id' => $request->sinif_id,
            'servis_id' => $request->servis_id,
        ]);

        $dene = Personel::create($personel);
        $path = public_path().'/uploads/staff/'.$dene->id;
        File::makeDirectory($path, $mode = 0777, true, true);
        return redirect()->action('Admin\PersonelController@personelEkle');
    }
    public function edit($id)
    {
        $userDelegation = AuthGroup::where('status',1)->get();
        $user = User::where('id', $id)->first();
        $personelResim = PersonelResim::where('personel_id', $id)->first();
        $sinif = Sinif::where('status',1)->get();
        $servis = Servis::where('status',1)->get();
        return view('admin.personal.edit', [
            'user' => $user,
            'userDelegation' => $userDelegation,
            'personelResim' => $personelResim,
            'sinif' => $sinif,
            'servis' => $servis
            ]);
    }
    public function update(Request $request)
    {
        $data = $request->all();
        if(empty($data['password']))
        {
            unset($data['password']);
        }
        else
        {
            $data['password'] = bcrypt($request->password);
        }
        //Personel ayrılıs yaptıysa şifresi değiştiriliyor.
        if($data['ayrilis_tarihi']){
          $data['password'] = bcrypt('sefkatrha');
        }
        // echo "<pre>";
        // print_r($data);
        // die();

        User::find($request->id)->update($data);
        return redirect()->back();
    }
    public function delete($id)
    {
        $student = User::where('id', $id)->first();
        $student->status = 0;
        $student->save();
        return redirect()->back();
    }
    public function profile($id)
    {
        $person = User::where('id', $id)->first();
        $allNotification = Notification::where('target_id', $id)->orderBy('created_at', 'desc')->get();
        $personelResim = PersonelResim::where('personel_id', $id)->first();
        /*echo "<pre>";
        echo($person->delegation->name);
        die();*/
        return view('admin.profile.index', ['person' => $person, 'notification' => $allNotification, 'personelResim' => $personelResim]);
    }
    public function profileSave(Request $request)
    {
        $data = $request->all();
        /*echo "<pre>";
        print_r($data);
        die();*/
        if(empty($data['password']))
        {
            unset($data['password']);
        }
        else
        {
            $data['password'] = bcrypt($request->password);
        }
        /*echo "<pre>";
        echo $request->id;
        print_r($data);
        die();*/
        User::find($request->user_id)->update($data);
        return redirect()->back();
    }
    public function savePersonelResim(Request $request)
    {
        $personelResim = $request->all();
        $personel_id = $request->input('personel_id');
        $personelResim = PersonelResim::where('personel_id', $personel_id)->first();
        /*$ogrenci_resim = $request->file('resim');
        $ogrenci_resim = $request->file('resim');*/

        $path = base_path() . '/public/uploads/staff/'.$personel_id;
        /*echo "<pre>";
        echo "SELAM";
        print_r($ogrenci_resim);
        die();*/
        $imageTempName = $request->file('resim')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('resim')->getClientOriginalName();

        $request->file('resim')->move($path , $imageName);
        $newresim = '/uploads/staff/'.$personel_id."/".$imageName;
        $data= array('personel_id'=> $personel_id, 'resim' => $newresim);
        $ogrencitopla = PersonelResim::updateOrCreate(['personel_id' => $personel_id],$data);

        return redirect()->back();
    }
}
