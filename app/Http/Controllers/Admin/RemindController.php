<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Remind;
use Auth;

class RemindController extends Controller
{
    public function delete($id)
    {
        $remind = Remind::where('id', $id)->first();
        $remind->status = 0;
        $remind->save();
        return redirect()->back();
    }
    public function save(Request $request)
    {
        $data = $request->all();
        $data['status'] = 1;
        $data['user_id'] = Auth::user()->id;
        // echo '<pre>';
        // var_dump($data);
        // die();
        Remind::create($data);
        return redirect()->back();
    }
}
