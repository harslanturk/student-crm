<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ogrenci;
use App\OgrenciResim;
use App\EngelTipleri;
use DB;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;
use File;
use App\StudentVisit;
use App\StudentVisitDetay;
use App\StudentVisitStatus;

class StudentController extends Controller
{

    public function ogrenciEkle()
    {
        if(helper::authControl('ogrenci-ekle','add')){
          $engelTipleri = EngelTipleri::all();
          return view('admin.ogrenci.ogrenci_ekle', ['engelTipleri' => $engelTipleri]);
        }
          return redirect()->action('Admin\HomeController@index');

    }
    public function addOgrenci()
    {
        $ogrenci = new Ogrenci();
        $ogrenci->ogrenci_no = $_POST['inputOgrenciNo'];
        $ogrenci->tcno = $_POST['inputTcNo'];
        $ogrenci->engel_tipi = $_POST['inputEngelTipi'];
        $ogrenci->ad = $_POST['inputAdi'];
        $ogrenci->soyad = $_POST['inputSoyadi'];
        $ogrenci->cinsiyet = $_POST['inputCinsiyeti'];
        $ogrenci->dogum_tarihi = $_POST['inputDogumTarihi'];
        $ogrenci->dogum_yeri = $_POST['inputDogumYeri'];
        $ogrenci->okul = $_POST['inputOkulunAdi'];
        $ogrenci->baba = $_POST['inputBabaAdi'];
        $ogrenci->baba_tel = $_POST['inputBabaTel'];
        $ogrenci->kayit_tarihi = $_POST['inputKayitTarihi'];
        $ogrenci->anne = $_POST['inputAnneAdi'];
        $ogrenci->anne_tel = $_POST['inputAnneTel'];
        $ogrenci->ayrilis_tarihi = $_POST['inputAyrilisTarihi'];
        $ogrenci->egitim = $_POST['inputEgitimSekli'];
        $ogrenci->diger_tel = $_POST['inputDigerTel'];
        $ogrenci->il = $_POST['inputIl'];
        $ogrenci->ilce = $_POST['inputIlce'];
        $ogrenci->adres = $_POST['inputAdres'];
        $ogrenci->status = 1;
        $ogrenci->save();
        Flash::message('Öğrenci başarılı bir şekilde eklendi.','success');
        $path = public_path().'/uploads/student/'.$ogrenci->id;
        File::makeDirectory($path, $mode = 0777, true, true);

        return redirect()->action('Admin\StudentController@listOgrenci');
    }
    public function listOgrenci()
    {
        if(helper::authControl('tum-ogrenciler','read')){
          $ogrenci = Ogrenci::where('status', 1)->where('ayrilis_tarihi', "0000-00-00")->orderBy('ad', 'ASC')->get();
          return view('admin.ogrenci.ogrenci_list',['ogrenci' => $ogrenci]);
        }
          return redirect()->action('Admin\HomeController@index');

    }
    public function searchOgrenci(Request $request)
    {
        $name=$request->input('query');
        $ogrenci = Ogrenci::where('ad', 'like', $name . '%')->orWhere('soyad', 'like', $name . '%')->orderBy('ad', 'ASC')->get();
        return view('admin.ogrenci.ogrenci_search_show',['ogrenci' => $ogrenci]);
    }
    public function saveOgrenci(Request $request)
    {
        $ogrenci = $request->all();
        Ogrenci::find($ogrenci['id'])->update($ogrenci);

        return redirect()->back();
       /* $ogrencitopla->update($request->all());
        $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci['id'])->first();
        $engelTipleri = EngelTipleri::all();*/
        /*Ogrenci::update($ogrenci);echo "<pre>";
        print_r($ogrenci);
        die();
        $ogrencitopla = Ogrenci::find($ogrenci['id'])->first();*/
        /*return view('admin.ogrenci.ogrenci_duzenle', ['ogrenci' => $ogrencitopla, 'ogrenciresim' => $ogrenciResim, 'engelTipleri' => $engelTipleri]);*/
    }
    public function saveOgrenciResim(Request $request)
    {
        $ogrenciResim = $request->all();
        $ogrenci_id = $request->input('ogrenci_id');
        /*$ogrenci_resim = $request->file('resim');
        $ogrenci_resim = $request->file('resim');*/
        if($request->file('resim') != null) {
            $path = base_path() . '/public/uploads/student/'.$ogrenci_id;
            /*echo "<pre>";
            echo "SELAM";
            print_r($ogrenci_resim);
            die();*/
            $imageTempName = $request->file('resim')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('resim')->getClientOriginalName();

            $request->file('resim')->move($path , $imageName);
            $newresim = '/uploads/student/'.$ogrenci_id."/".$imageName;
            $data= array('ogrenci_id'=> $ogrenci_id, 'resim' => $newresim);
            $ogrencitopla = OgrenciResim::updateOrCreate(['ogrenci_id' => $ogrenci_id],$data);
        }

        return redirect()->back();
    }
    public function detayOgrenci($id)
    {
        $ogrenci = Ogrenci::where('id', $id)->first();
        $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci->id)->first();
        //$islem_detay = IslemDetay::where('status', 1);
        $islem_detay = DB::table('islem_detay')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->join('users', 'islem_detay.alici_id', '=', 'users.id')
            ->where('islem_detay.ogrenci_id', $id)
            ->orderBy('created_at', 'desc')
            ->select('islem_detay.*','ogrenci_islemleri.islem as konu_islem','users.name as sorumlu')
            ->get();
        $islem_detay_sp = DB::table('islem_detay')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->where('islem_detay.ogrenci_id', $id)
            ->where('islem_detay.status', 1)
            ->select('ogrenci_islemleri.islem as konu_islem')
            ->get();
        /*echo "<pre>";
        print_r($islem_detay_sp);
        die();*/
        return view('admin.ogrenci.ogrenci_detay',[
          'ogrenci' => $ogrenci,
          'islemDetay' => $islem_detay,
          'islemDetaySp' => $islem_detay_sp,
          'ogrenciResim' => $ogrenciResim
        ]);
    }
    public function duzenleOgrenci($id="1")
    {
        $ogrenci = Ogrenci::where('id', $id)->first();
        $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci->id)->first();
        $engelTipleri = EngelTipleri::all();
        $islem_detay = DB::table('islem_detay')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->join('users', 'islem_detay.alici_id', '=', 'users.id')
            ->where('islem_detay.ogrenci_id', $id)
            ->orderBy('created_at', 'desc')
            ->select('islem_detay.*','ogrenci_islemleri.islem as konu_islem','users.name as sorumlu')
            ->get();
            $islem_detay = count($islem_detay);
            // echo "<pre>";
            // print_r(count($islem_detay));
            // die();
        return view('admin.ogrenci.ogrenci_duzenle',['ogrenci' => $ogrenci, 'ogrenciResim' => $ogrenciResim, 'engelTipleri' => $engelTipleri,'islem_detay' => $islem_detay]);
    }
    public function delete($id)
    {
        $student = Ogrenci::where('id', $id)->first();
        $student->status = 0;
        $student->save();
        return redirect()->back();
    }

    public function leavingOgrenci()
    {
        $ogrenci = Ogrenci::where('status', 1)->where('ayrilis_tarihi','!=','0000-00-00')->get();
        return view('admin.ogrenci.ogrenci_ayrilan',['ogrenci' => $ogrenci]);
    }

    public function ayrilOgrenci($id)
    {
        $bugun = date("Y-m-d");
        $ogrenci = Ogrenci::where('id', $id)->first();
        $ogrenci->ayrilis_tarihi = $bugun;
        $ogrenci->save();
        return redirect()->back();
    }

    public function aktifOgrenci($id)
    {
        $bugun = "0000-00-00";
        $ogrenci = Ogrenci::where('id', $id)->first();
        $ogrenci->ayrilis_tarihi = $bugun;
        $ogrenci->save();
        return redirect()->back();
    }
    public function familyShow($id)
    {
      $ogrenci = Ogrenci::where('id', $id)->first();
      $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci->id)->first();
      $family = DB::table('student_visit')
                    ->join('users','users.id','=','student_visit.alici_id')
                    ->join('ogrenci','ogrenci.id','=','student_visit.ogrenci_id')
                    ->where('student_visit.ogrenci_id', $id)
                    ->where('student_visit.status',1)
                    ->select('student_visit.*','ogrenci.ad as o_ad','users.name as alici_ad')
                    ->get();
      //$islem_detay = IslemDetay::where('status', 1);

      // echo "<pre>";
      // print_r($family);
      // echo count($family);
      // die();
      return view('admin.ogrenci.family',
      [
        'ogrenci' => $ogrenci,
        'ogrenciResim' => $ogrenciResim,
        'familys' => $family
      ]);
    }
    public function saveFamily(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      // echo "<pre>";
      // print_r($data);
      // die();
      StudentVisit::create($data);
      return redirect()->back();
    }

    public function familyShowDetay($id,$o_id)
    {

      $family = DB::table('student_visit')
                    ->join('users','users.id','=','student_visit.alici_id')
                    ->join('ogrenci','ogrenci.id','=','student_visit.ogrenci_id')
                    ->where('ogrenci.status',1)
                    ->where('users.status',1)
                    ->where('student_visit.id',$id)
                    ->select('student_visit.*','ogrenci.ad as o_ad','users.name as alici_ad')
                    ->first();
      $ogrenci = Ogrenci::where('id', $o_id)->first();
      $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci->id)->first();
      $family_detay = DB::table('student_visit_detay')
                        ->join('student_visit_status','student_visit_status.id','=','student_visit_detay.durum')
                        ->where('student_visit_id',$id)
                        ->select('student_visit_detay.*','student_visit_status.islem')
                        ->get();

      $studentVisitStatus = StudentVisitStatus::all();

      return view('admin.ogrenci.family_detay',
      [
        'ogrenci' => $ogrenci,
        'ogrenciResim' => $ogrenciResim,
        'familys' => $family,
        'family_detays' => $family_detay,
        'studentVisitStatus' => $studentVisitStatus
      ]);
    }

    public function saveFamilyDetay(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;

      StudentVisitDetay::create($data);
      return redirect()->back();
    }
}
