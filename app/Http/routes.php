<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::auth();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'Admin\HomeController@index');
    /***** Öğrenci Ekle Modülü *****/
    Route::get('/admin/students', 'Admin\StudentController@listOgrenci');
    Route::get('/admin/students/list', 'Admin\StudentController@listOgrenci');
    Route::get('/admin/students/create', 'Admin\StudentController@ogrenciEkle');
    Route::get('/admin/students/show/{id?}', 'Admin\StudentController@detayOgrenci');
    Route::get('/admin/students/edit/{id?}', 'Admin\StudentController@duzenleOgrenci');
    Route::get('/admin/students/delete/{id?}', 'Admin\StudentController@delete');
    Route::get('/admin/students/active/{id?}', 'Admin\StudentController@aktifOgrenci');
    Route::get('/admin/students/leave/{id?}', 'Admin\StudentController@ayrilOgrenci');
    Route::get('/admin/students/leaving', 'Admin\StudentController@leavingOgrenci');
    Route::get('/admin/students/family-show/{id?}', 'Admin\StudentController@familyShow');
    Route::get('/admin/students/family-show-detay/{id?}/{o_id?}', 'Admin\StudentController@familyShowDetay');

    Route::post('/admin/students/new', 'Admin\StudentController@addOgrenci');
    Route::post('/admin/students/update', 'Admin\StudentController@saveOgrenci');
    Route::post('/admin/students/search', 'Admin\StudentController@searchOgrenci');
    Route::post('/admin/students/update-resim', 'Admin\StudentController@saveOgrenciResim');
    Route::post('/admin/students/saveFamily', 'Admin\StudentController@saveFamily');
    Route::post('/admin/students/saveFamilyDetay', 'Admin\StudentController@saveFamilyDetay');
    /***** Öğrenci Ekle Modülü Bitiş *****/

    /***** Misafir Öğrenci Ekle Modülü *****/
    Route::get('/admin/guest-students', 'Admin\GuestStudentController@listOgrenci');
    Route::get('/admin/guest-students/list', 'Admin\GuestStudentController@listOgrenci');
    Route::get('/admin/guest-students/create', 'Admin\GuestStudentController@ogrenciEkle');
    Route::get('/admin/guest-students/show/{id?}', 'Admin\GuestStudentController@detayOgrenci');
    Route::get('/admin/guest-students/edit/{id?}', 'Admin\GuestStudentController@duzenleOgrenci');
    Route::get('/admin/guest-students/delete/{id?}', 'Admin\GuestStudentController@delete');

    Route::post('/admin/guest-students/new', 'Admin\GuestStudentController@addOgrenci');
    Route::post('/admin/guest-students/update', 'Admin\GuestStudentController@saveOgrenci');
    Route::post('/admin/guest-students/update-resim', 'Admin\GuestStudentController@saveOgrenciResim');
    /***** Misafir Öğrenci Ekle Modülü Bitiş *****/

    /***** Personel Ekle Modülü *****/
    Route::get('/admin/staff', 'Admin\PersonelController@listPersonel');
    Route::get('/admin/staff/list', 'Admin\PersonelController@listPersonel');
    Route::get('/admin/staff/create', 'Admin\PersonelController@personelEkle');
    Route::get('/admin/staff/edit/{id?}', 'Admin\PersonelController@edit');
    Route::get('/admin/staff/profile/{id}','Admin\PersonelController@profile'); //Profil Göster
    Route::get('/admin/staff/delete/{id}','Admin\PersonelController@delete'); // Personel Sil

    Route::post('/admin/staff/profile/save','Admin\PersonelController@profileSave'); //Profil Göster
    Route::post('/admin/staff/new', 'Admin\PersonelController@addPersonel');
    Route::post('/admin/staff/update', 'Admin\PersonelController@update');
    Route::post('/admin/staff/update-resim', 'Admin\PersonelController@savePersonelResim');
    /***** Personel Ekle Modülü Bitiş *****/

    /***** Mesajlar Modülü *****/
    Route::get('/mesajlar', 'Admin\SupportController@listMessage');
    Route::get('/mesajlar/mesaj-olustur', 'Admin\SupportController@composeMessage');
    Route::get('/mesajlar/giden-kutusu', 'Admin\SupportController@gidenMessage');
    Route::get('/mesajlar/mesaj-goster/{id?}', 'Admin\SupportController@showMessage');

    Route::post('/mesajGonder', 'Admin\SupportController@sendMessage');
    Route::post('/replyMessage', 'Admin\SupportController@cevapMessage');
    /***** Mesajlar Modülü Bitiş *****/

    /***** İşlem Modülü *****/
    Route::get('/admin/process/show/{id?}', 'Admin\ProcessController@detayIslem');
    Route::get('/admin/process/detail/{id?}', 'Admin\ProcessController@islemDetayOgrenci');
    Route::get('/admin/process/open/{id?}', 'Admin\ProcessController@openKonu');
    Route::get('/admin/process/mywork', 'Admin\ProcessController@myWork');

    Route::post('/admin/process/islemcek', 'Admin\ProcessController@islemCek');
    Route::post('/admin/process/saveKonu', 'Admin\ProcessController@saveKonu');
    Route::post('/admin/process/saveIslem', 'Admin\ProcessController@saveIslem');
    Route::post('/admin/process/search', 'Admin\ProcessController@searchIslem');
    /***** İşlem Modülü Bitiş *****/

    /***** Tanımlama Modülü *****/
    Route::get('/ogrenci-islemleri', 'Admin\DefineController@ogrenciIslemleri');
    Route::get('/engel-tipleri', 'Admin\DefineController@engelTipleri');
    Route::get('/student-visit-status', 'Admin\DefineController@studentVisitStatus');
    Route::get('/sinif', 'Admin\DefineController@sinifIndex');
    Route::get('/servis', 'Admin\DefineController@servisIndex');
    Route::get('/plan-group', 'Admin\DefineController@planGrupIndex');

    Route::post('/ogrenciIslemEkle', 'Admin\DefineController@addOgrenciIslem');
    Route::post('/engelTipiEkle', 'Admin\DefineController@addEngelTipi');
    Route::post('/studentVisitStatusEkle', 'Admin\DefineController@addStudentVisitStatus');
    Route::post('/sinifEkle', 'Admin\DefineController@addSinif');
    Route::post('/servisEkle', 'Admin\DefineController@addServis');
    Route::post('/planGrupEkleModal', 'Admin\DefineController@PlanGroupModal');
    Route::post('/planGrupEkle', 'Admin\DefineController@addPlanGroup');

    Route::post('/engel-tip-guncelle', 'Admin\DefineController@editEngelTipi');
    Route::post('/engel-tip-save', 'Admin\DefineController@saveEngelTipi');
    Route::post('/ogrenci-islem-guncelle', 'Admin\DefineController@editOgrenciIslem');
    Route::post('/ogrenci-islem-save', 'Admin\DefineController@saveOgrenciIslem');
    Route::post('/aile-gorusme-guncelle', 'Admin\DefineController@editStudentVisitStatus');
    Route::post('/student-visit-status-save', 'Admin\DefineController@saveStudentVisitStatus');
    Route::post('/sinif-guncelle', 'Admin\DefineController@editSinif');
    Route::post('/sinif-save', 'Admin\DefineController@saveSinif');
    Route::post('/servis-guncelle', 'Admin\DefineController@editServis');
    Route::post('/servis-save', 'Admin\DefineController@saveServis');
    Route::post('/planGrupEdit', 'Admin\DefineController@planGrupEdit');
    Route::post('/planGrupUpdate', 'Admin\DefineController@planGrupUpdate');

    /***** Tanımlama Modülü Bitiş *****/

    /***** Silme Modülü *****/
    Route::get('/sil/{tablo?}/{id?}', 'Admin\DeleteController@simpleDelete');
    /***** Silme Modülü Bitiş *****/

    /*** Ayarlar Modülü Başlangıcı ***/
    Route::get('/admin/settings','Admin\SettingsController@index');
    Route::post('/admin/settings/save','Admin\SettingsController@save');
    /*Ayarlar Modülü Bitişi*/

    /*** Yetkilendirme Modülü ***/
    Route::get('/admin/delegations','Admin\UserDelegationController@index');
    Route::get('admin/delegations/create','Admin\UserDelegationController@create_file');
    Route::get('admin/delegations/edit/{id}','Admin\UserDelegationController@update_file');
    Route::get('admin/delegations/delete/{id}','Admin\UserDelegationController@delete');

    Route::post('admin/delegations/ajaxDelegationAdd','Admin\UserDelegationController@ajax_createFile');
    Route::post('/admin/delegations/ajaxDelegationEdit','Admin\UserDelegationController@ajax_editFile');
    Route::get('/admin/delegations/delegationDelete','Admin\UserDelegationController@ajax_deleteFile');
    /*** Yetkilendirme Modülü Son ***/

    /*** Modüller Modülü ***/
    Route::get('/admin/modules','Admin\ModulesController@index');
    Route::get('admin/modules/create','Admin\ModulesController@create_file');
    Route::get('/admin/modules/edit/{id}','Admin\ModulesController@edit_file');

    Route::post('/admin/modules/ajaxModulesCreate','Admin\ModulesController@ajax_createFile');
    Route::post('/admin/modules/ajaxModulesEdit','Admin\ModulesController@ajax_editFile');
    Route::get('/admin/modules/delete/{id}','Admin\ModulesController@ajax_deleteFile');
    /*** Modüller Modülü Son ***/

    /*** İstatistik Modülü ***/
    Route::get('/admin/stats','Admin\StatsController@index');
    Route::get('/admin/stats/{year?}','Admin\StatsController@indexYear');
    Route::get('/admin/updateList','Admin\StatsController@update');

    Route::post('/admin/stats/year','Admin\StatsController@postYear');
    /*** İstatistik Modülü Bitiş ***/

    /*** Sms Modülü Başlangıç ***/
    Route::get('/admin/sms','Admin\SmsController@index'); //Tüm Hastalar Sayfası
    Route::get('/admin/sms/create','Admin\SmsController@create'); //Yeni Hasta Ekleme
    Route::get('/admin/sms/edit/{id}','Admin\SmsController@edit');//Hasta Düzenleme
    Route::get('/admin/sms/delete/{id}','Admin\SmsController@delete'); //Hasta Silme

    Route::post('/admin/sms/create','Admin\SmsController@createPost'); //Sms Atma Tablodan Seçerek
    Route::post('/admin/sms/elle','Admin\SmsController@createElle'); //Sms Atma Elle Numara Girerek
    Route::post('/admin/sms/save','Admin\SmsController@save'); //Hastayı Kaydetme
    /*** Sms Modülü Bitiş ***/

    /*** Duyuru Modülü ***/
    Route::post('/admin/notice/create','Admin\NoticeController@create'); //Duyuru Ekleme
    Route::post('/admin/notice/upload','Admin\NoticeController@fileUpload'); //Duyuru Yükleme
    Route::post('/admin/muhasebe/notice-detay/show','Admin\NoticeController@getNotice'); //Duyuru Düzenleme
    Route::get('/admin/notice/delete/{id?}', 'Admin\NoticeController@delete'); //Duyuru Silme
    /*** Duyuru Modülü Bitiş ***/

    /*** Notifikasyon Modülü ***/
    Route::get('/loadNotification', 'NotificationController@loadNotification');

    Route::post('/deleteNtfn', 'NotificationController@deleteNtfn');
    /*** Notifikasyon Modülü Bitiş ***/

    /*** Hatırlatma Modülü ***/
    Route::get('/admin/remind/delete/{id}','Admin\RemindController@delete'); //Hatırlatma Sil

    Route::post('/admin/remind/save','Admin\RemindController@save'); //Hatırlatma Ekle
    /*** Hatırlatma Modülü Bitiş ***/

    /*** Danışman Modülü ***/
    Route::get('/admin/advisor','Admin\AdvisorController@index'); //Danışman Listele
    Route::get('/admin/advisor/delete/{id?}', 'Admin\AdvisorController@delete'); //Duyuru Silme
    Route::get('/admin/advisor/read/{id?}', 'Admin\AdvisorController@read'); //Duyuru Okuma

    Route::post('/admin/advisor/save','Admin\AdvisorController@save'); //Danışman Ekle
    /*** Danışman Modülü Bitiş ***/

    /*** Mesajlar Modülü ***/
    Route::get('/admin/chat/{receiver?}', 'Admin\ChatController@showMessage');
    Route::get('/admin/chat/loadMessage/{receiver?}/{lastid?}', 'Admin\ChatController@loadMessage');
    Route::get('/admin/chat/loadMessageLast', 'Admin\ChatController@loadMessageLast');

    Route::post('/admin/chat/save', 'Admin\ChatController@saveMessage');
    /*** Mesajlar Modülü Bitiş ***/

    /***** Test Modülü *****/
    Route::get('/test', 'HomeController@test');
    /***** Test Modülü Bitiş *****/

    // Auth işlemleri
    Route::get('/admin/authorization','Admin\AuthorizationController@index');
    Route::get('/admin/getAuthorization/{id}', 'Admin\AuthorizationController@getAuth');
    Route::get('/admin/authorization/deleteAuthGroup/{id}', 'Admin\AuthorizationController@deleteAuthGroup');

    Route::post('/admin/authorization/createAuth', 'Admin\AuthorizationController@createAuth');
    Route::post('/admin/authorization/createAuthGroup', 'Admin\AuthorizationController@createAuthGroup');
    //

    //Planlama işlemleri
    Route::get('/admin/planlama','Admin\PlanlamaController@index2');
    Route::get('/admin/planlama/api','Admin\PlanlamaController@api');
    Route::get('/admin/planlama/delete/{id}','Admin\PlanlamaController@delete');

    Route::post('/admin/planlama/createCalendarModalShow','Admin\PlanlamaController@createCalendarModalShow');
    Route::post('/admin/planlama/calendarModalSave','Admin\PlanlamaController@calendarModalSave');
    Route::post('/admin/planlama/updateEventDrop','Admin\PlanlamaController@updateEventDrop');
    Route::post('/admin/planlama/editModal','Admin\PlanlamaController@editModal');
    Route::post('/admin/planlama/updateCalendar/{id}','Admin\PlanlamaController@update');

    //Programlar
    Route::get('/admin/program/show/{id}','Admin\PlanlamaController@showProgram');
    Route::get('/admin/program/edit/{id}','Admin\PlanlamaController@editProgram');

    Route::post('/admin/planlama/createProgramModal','Admin\PlanlamaController@createProgramModal');
    Route::post('/admin/planlama/saveProgramModal','Admin\PlanlamaController@saveProgramModal');
    Route::post('/admin/program/createProgram','Admin\PlanlamaController@createProgram');
    Route::post('/admin/program/ogrenci-modal','Admin\PlanlamaController@ogrenciModal');
    Route::post('/admin/program/ogrenci-modal-save','Admin\PlanlamaController@ogrenciModalSave');
    Route::post('/admin/program/CopyModal','Admin\PlanlamaController@CopyModal');
    Route::post('/admin/program/copySave','Admin\PlanlamaController@copySave');

    Route::post('/admin/program/filterModal','Admin\PlanlamaController@filterModal');
    Route::post('/admin/program/filter','Admin\PlanlamaController@filter');

    //Plan Program Yazdır.Servis İçin
    Route::get('/admin/yazdir/servis','Admin\PlanlamaController@indexServis');
    Route::get('/admin/yazdir/servis/{id}','Admin\PlanlamaController@indexServisPrint');
    Route::get('/admin/yazdir/servis/print/{id}/{servis_id}','Admin\PlanlamaController@ServisPrint');

    //Sınıf İçin
    Route::get('/admin/yazdir/ogrenci','Admin\PlanlamaController@indexSinif');
    Route::get('/admin/yazdir/sinif/{id}','Admin\PlanlamaController@indexSinifPrint');
    Route::get('/admin/yazdir/sinif/print/{id}/{sinif_id}','Admin\PlanlamaController@SinifPrint');

});
