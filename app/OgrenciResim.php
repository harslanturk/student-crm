<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OgrenciResim extends Model
{
    protected $table = "ogrenci_resim";
    protected $fillable = [
        'ogrenci_id', 'resim',
    ];
}
