<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EngelTipleri extends Model
{
    protected $table = "engel_tipleri";
    protected $fillable = [
        'engel',
    ];
}
