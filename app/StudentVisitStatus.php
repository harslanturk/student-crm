<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentVisitStatus extends Model
{
  protected $table = "student_visit_status";
  protected $fillable = [
      'islem',
  ];
}
