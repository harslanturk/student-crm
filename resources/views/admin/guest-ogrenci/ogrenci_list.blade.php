@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- ALERT -->
@if (Session::has('flash_notification.message'))
  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('flash_notification.message') }}
  </div>
@endif
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Tüm Misafir Öğrenciler
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Misafir Öğrenci Listesi</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
      <div class="box-header">
        <h3 class="box-title">
          @if(App\Helpers\helper::authControl('misafir-ogrenci-ekle','add'))
          <a href="/admin/guest-students/create"><button class="btn btn-primary pull-right" style=""><b>Yeni Misafir Öğrenci</b></button></a>
          @endif
        </h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="ogrenci-table" class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th>Sıra</th>
              <th>Öğrenci No</th>
              <th>Adı</th>
              <th>Soyadı</th>
              <th>TC No</th>
              <th>Cinsiyet</th>
              <th>Engel Tipi</th>
            </tr>
          </thead>
          <tbody>
          <?php $sira=1; ?>
          @foreach($ogrenci as $student)
            <tr onclick="document.location = '/admin/guest-students/show/{{ $student->id }}';" style="cursor: pointer;">
              <td>{{ $sira }}</td>
              <td>{{ $student->ogrenci_no }}</td>
              <td>{{ $student->ad }}</td>
              <td>{{ $student->soyad }}</td>
              <td>{{ $student->tcno }}</td>
              <td>{{ $student->cinsiyet }}</td>
              <td>{{ $student->engel_tipi }}</td>
            </tr>
            <?php $sira++; ?>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Sıra</th>
              <th>Öğrenci No</th>
              <th>Adı</th>
              <th>Soyadı</th>
              <th>TC No</th>
              <th>Cinsiyet</th>
              <th>Engel Tipi</th>
            </tr>
          </tfoot>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()
