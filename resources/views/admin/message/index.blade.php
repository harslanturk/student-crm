@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- ALERT -->
@if (Session::has('flash_notification.message'))
  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('flash_notification.message') }}
  </div>
@endif
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Mesajlar
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Mesajlar</li>
  </ol>
</section>
<?php $user = Auth::user(); ?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <style>
        .affix {
          top: 70px;
          position: fixed !important;
        }
      </style>
      <div class="row">
        <div class="col-sm-3">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-users"></i> Konuşmalar</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group chat-left-menu scroll-type-1" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                @if($status==1)
                  <div class="panel box" style="border-left:20px solid #3c8dbc;">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a href="/admin/chat/{{ $receiverUsername }}">
                          {{ $receiverUsername }}
                        </a>
                      </h4>
                    </div>
                  </div>
                  @foreach($contactList as $value)
                    @if(($receiverUsername != $value->name))
                      @if(($value->name != $user->name))
                        <div class="panel box">
                          <div class="box-header with-border">
                            <h4 class="box-title">
                              <a href="/admin/chat/{{ $value->id }}">
                                {{$value->name}}
                              </a>
                            </h4>
                          </div>
                        </div>
                      @endif
                    @endif
                  @endforeach
                @else
                  @foreach($contactList as $value)
                    @if(($value->name != $user->name))
                    <div class="panel box">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a href="/admin/chat/{{ $value->id }}">
                            {{$value->name}}
                          </a>
                        </h4>
                      </div>
                    </div>
                    @endif
                  @endforeach
                @endif
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-sm-9">
          <!-- Chat box -->
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-comments-o"></i>
              <h3 class="box-title">Chat</h3>
              <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                <div class="btn-group" data-toggle="btn-toggle" >
                  <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                </div>
              </div>
            </div>
            <div class="box-body chat showMessage" id="chat-box" style="background-color:#eee;">
              <!-- chat item -->
              <div class="row showMessageLast" id="none-effect">
                @if($status=="1")
                  <input type="hidden" id="lastId" value="{{ $lastMessage[0]->id }}">
                  @foreach($oldMessage as $oldmsg)
                    <?php
                    $user=Auth::user();
                    session(['lastMessageId', $oldmsg->id]);
                    ?>
                    @if($oldmsg->sender == $user->id)
                      <div class="item" style="border: 1px solid #00a65a;background-color: #00a65a;border-radius: 5px;padding: 5px; width:60%; float:right;">
                        <img src="http://placehold.it/160x160" alt="user image" class="online" style="border: 2px solid #FFF;">
                        <p class="message" style="color:#FFF;">
                          <a href="#" class="name" style="color:#FFF !important;">
                            <small class="text-muted pull-right" style="color:#FFF;"><i class="fa fa-clock-o"></i> <?php
                              $date1 = new DateTime($oldmsg->created_at);
                              $date1Cikti = $date1->format('H:i');
                              $date2 = \Carbon\Carbon::now();
                              $diff=date_diff($date2,$date1);
                              if($diff->d<1)
                              {
                                $result=" Bugün";
                              }
                              elseif($diff->d<2){
                                $result=" Dün";
                              }
                              else{
                                $result=$diff->d." Gün Önce";
                              }
                              ?>{{ $date1Cikti }} {{ $result }}</small>
                            {{$oldmsg->sender_username}}
                          </a>
                          {{ $oldmsg->content }}<br>
                        </p>
                      </div><!-- /.item -->
                    @else
                      <div class="item" style="border: 1px solid #fff;background-color: #fff;border-radius: 5px; padding:5px;width:60%; float:left;">
                        <img src="http://placehold.it/160x160" alt="user image" class="online">
                        <p class="message">
                          <a href="#" class="name">
                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php
                              $date1 = new DateTime($oldmsg->created_at);
                              $date1Cikti = $date1->format('H:i');
                              $date2 = \Carbon\Carbon::now();
                              $diff=date_diff($date2,$date1);
                              if($diff->d<1)
                              {
                                $result=" Bugün";
                              }
                              elseif($diff->d<2){
                                $result=" Dün";
                              }
                              else{
                                $result=$diff->d." Gün Önce";
                              }
                              ?>{{ $date1Cikti }} {{ $result }}</small>
                            {{$oldmsg->sender_username}}
                          </a>
                          {{ $oldmsg->content }}<br>
                        </p>
                      </div><!-- /.item -->
                    @endif
                  @endforeach
                @elseif($status=="2")
                  <input type="hidden" id="lastId" value="0">
                  <div class="item" style="border: 1px solid #00a65a;background-color: #00a65a;border-radius: 5px;padding: 5px; width:60%; float:right;">
                    <img src="http://placehold.it/160x160" alt="user image" class="online" style="border: 2px solid #FFF;">
                    <p class="message" style="color:#FFF;">
                      <a href="#" class="name" style="color:#FFF !important;">
                        <small class="text-muted pull-right" style="color:#FFF;"><i class="fa fa-clock-o"></i> <?php
                          $date1 = new DateTime();
                          $date1Cikti = $date1->format('H:i');
                          $date2 = \Carbon\Carbon::now();
                          $diff=date_diff($date2,$date1);
                          if($diff->d<1)
                          {
                            $result=" Bugün";
                          }
                          elseif($diff->d<2){
                            $result=" Dün";
                          }
                          else{
                            $result=$diff->d." Gün Önce";
                          }
                          ?>{{ $date1Cikti }} {{ $result }}</small>
                        {{ $user->name }}
                      </a>
                      Daha önce mesajlaşmadınız.
                    </p>
                  </div><!-- /.item -->
                @else
                  <!--ALO3-->
                @endif
              </div>

            </div><!-- /.chat -->
            <div class="box-footer">
              <div class="input-group">
                @if($status==1)
                  <input class="form-control" placeholder="Mesaj Giriniz..." id="pvMessage">
                  <div class="input-group-btn">
                    <button class="btn btn-success" onclick="sendMessage('{{ $receiver }}', '{{ $lastMessage[0]->id }}');" id="pvMessageBttn"><i class="fa fa-plus"></i></button>
                  </div>
                @elseif($status==2)
                  <input class="form-control" placeholder="Mesaj Giriniz..." id="pvMessage">
                  <div class="input-group-btn">
                    <button class="btn btn-success" onclick="sendMessage('{{ $receiver }}', '0');" id="pvMessageBttn"><i class="fa fa-plus"></i></button>
                  </div>
                @elseif($status==0)
                  Mesaj göndermek için sol taraftan birini seçiniz.
                @endif
              </div>
            </div>

          </div><!-- /.box (chat box) -->
        </div>
      </div>
      <?php /*$deger=session('lastMessageId'); echo "<h1>BURADA OLMASI LAZIM =".$deger."</h1>"; die(); */?>
      @if($status==1 ||$status==2)
        <script>
          $('#chat-box').slimScroll({
            height: '400px',
            railOpacity: 0.9,
            start: 'bottom'
          });
          $('#chat-box').slimScroll({ scrollBy: $('#none-effect').height() });
          setTimeout(loadmessage, 10000);
          function loadmessage(receiver, lastid){
            var lastidInput = document.getElementById('lastId').value;
            lastid = lastid || lastidInput;
            $.get('/admin/chat/loadMessage/{{ $receiver }}/'+lastid, function(data){
              content= data;
              $('.showMessageLast').append(content);
            });
            /*
             $(".showMessage").append(load("/loadMessage/{{ $receiver }}"));*/
            setTimeout(loadmessage, 10000);
            /*$('#chat-box').slimScroll({ scrollBy: $('#none-effect').height() });*/
          }
          $('#pvMessage').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
              $('#pvMessageBttn').click();
              return false;
            }
          });
          function sendMessage(receiver,lastid)
          {
            console.log("burdayız "+$('meta[name="csrf_token"]').attr('content'));
            var pvMessage = document.getElementById('pvMessage').value;
            $("#pvMessage").prop('disabled', true);
            $("#pvMessageBttn").prop('disabled', true);
            $.ajax({
              url: '/admin/chat/save',
              type: 'POST',
              beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
              },
              cache: false,
              data: {pvMessage: pvMessage, receiver: receiver },
              success: function(data){
                loadmessage(receiver,lastid);
                $('#pvMessage').val(" ");
                $("#pvMessage").prop('disabled', false);
                $("#pvMessageBttn").prop('disabled', false);
                $('#chat-box').slimScroll({ scrollBy: $('#none-effect').height() });
              },
              error: function(jqXHR, textStatus, err){}
            })
          }
        </script>
      @endif
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()