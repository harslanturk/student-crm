@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    İstatistikler
    <small>İstatistik Listesi</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><a href="/admin/stats"><i class="fa fa-dashboard active"></i>İstatistikler</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
  @if(App\Helpers\helper::authControl('personel-istatistik','read'))
		<div class="col-md-6">
            <!-- DONUT CHART -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Personel Sayısı</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="pieChart" style="height:250px"></canvas>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
		</div>
    @endif
    @if(App\Helpers\helper::authControl('ogrenci-istatistik','read'))
        <div class="col-md-6">
            <!-- DONUT CHART -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Öğrenci Dağılımı</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="pieChart2" style="height:250px"></canvas>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
		</div>
    @endif
        <div class="col-md-12">

            <div class="col-md-4 padding-temizle" style="margin-bottom:20px;">
                <form action="/admin/stats/year" method="POST">
                {{ csrf_field() }}
                <div class="input-group input-group-sm">
                    <input type="number" class="form-control" name="year" id="name" placeholder="Lütfen Yıl Giriniz. Ör:2017">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat">Git!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
        @if(App\Helpers\helper::authControl('ogrenci-konu-istatistik','read'))
        <div class="col-md-12">
            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$year}} Yılı Öğrenci Konuları</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="barChart" style="height:230px"></canvas>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        @endif
        @if(App\Helpers\helper::authControl('kayit-yapilan-ogrenci-istatistik','read'))
        <div class="col-md-12">
            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$year}} Yılı Yeni Öğrenci</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="barChart2" style="height:230px"></canvas>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        @endif
        <div class="col-md-12">
            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$year}} Yılı Ayrılan Öğrenci</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="barChart3" style="height:230px"></canvas>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
	</div>
</section><!-- /.content -->

</div><!-- /.content-wrapper -->
<script>
    $( document ).ready(function() {
        var areaChartData = {
            labels: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            datasets: [
                {
                    label: "Açılan Konular",
                    fillColor: "rgba(210, 214, 222, 1)",
                    strokeColor: "rgba(210, 214, 222, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [{{$ocakIslemDetay}}, {{$subatIslemDetay}}, {{$martIslemDetay}}, {{$nisanIslemDetay}}, {{$mayisIslemDetay}}, {{$haziranIslemDetay}}, {{$temmuzIslemDetay}}, {{$agustosIslemDetay}}, {{$eylulIslemDetay}}, {{$ekimIslemDetay}}, {{$kasimIslemDetay}}, {{$aralikIslemDetay}}]
                },
                {
                    label: "Reddedilen Konular",
                    fillColor: "rgba(221,75,57,1)",
                    strokeColor: "rgba(221,75,57,1)",
                    pointColor: "#DD4B39",
                    pointStrokeColor: "rgba(221,75,57,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(221,75,57,1)",
                    data: [{{$ocakIslemDetayRed}}, {{$subatIslemDetayRed}}, {{$martIslemDetayRed}}, {{$nisanIslemDetayRed}}, {{$mayisIslemDetayRed}}, {{$haziranIslemDetayRed}}, {{$temmuzIslemDetayRed}}, {{$agustosIslemDetayRed}}, {{$eylulIslemDetayRed}}, {{$ekimIslemDetayRed}}, {{$kasimIslemDetayRed}}, {{$aralikIslemDetayRed}}]
                },
                {
                    label: "Onaylanan Konular",
                    fillColor: "rgba(60,141,188,0.9)",
                    strokeColor: "rgba(60,141,188,0.8)",
                    pointColor: "#3b8bba",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: [{{$ocakIslemDetayOnay}}, {{$subatIslemDetayOnay}}, {{$martIslemDetayOnay}}, {{$nisanIslemDetayOnay}}, {{$mayisIslemDetayOnay}}, {{$haziranIslemDetayOnay}}, {{$temmuzIslemDetayOnay}}, {{$agustosIslemDetayOnay}}, {{$eylulIslemDetayOnay}}, {{$ekimIslemDetayOnay}}, {{$kasimIslemDetayOnay}}, {{$aralikIslemDetayOnay}}]
                }
            ]
        };
        var areaChartData2 = {
            labels: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            datasets: [
                {
                    label: "Bay",
                    fillColor: "rgba(210, 214, 222, 1)",
                    strokeColor: "rgba(210, 214, 222, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [{{$ocakYeniOgrenciErkek}}, {{$subatYeniOgrenciErkek}}, {{$martYeniOgrenciErkek}}, {{$nisanYeniOgrenciErkek}}, {{$mayisYeniOgrenciErkek}}, {{$haziranYeniOgrenciErkek}}, {{$temmuzYeniOgrenciErkek}}, {{$agustosYeniOgrenciErkek}}, {{$eylulYeniOgrenciErkek}}, {{$ekimYeniOgrenciErkek}}, {{$kasimYeniOgrenciErkek}}, {{$aralikYeniOgrenciErkek}}]
                },
                {
                    label: "Bayan",
                    fillColor: "rgba(60,141,188,0.9)",
                    strokeColor: "rgba(60,141,188,0.8)",
                    pointColor: "#3b8bba",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: [{{$ocakYeniOgrenciKız}}, {{$subatYeniOgrenciKız}}, {{$martYeniOgrenciKız}}, {{$nisanYeniOgrenciKız}}, {{$mayisYeniOgrenciKız}}, {{$haziranYeniOgrenciKız}}, {{$temmuzYeniOgrenciKız}}, {{$agustosYeniOgrenciKız}}, {{$eylulYeniOgrenciKız}}, {{$ekimYeniOgrenciKız}}, {{$kasimYeniOgrenciKız}}, {{$aralikYeniOgrenciKız}}]
                }
            ]
        };
        var areaChartData3 = {
            labels: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            datasets: [
                {
                    label: "Bay",
                    fillColor: "rgba(210, 214, 222, 1)",
                    strokeColor: "rgba(210, 214, 222, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [{{$ocakAyrilanOgrenciErkek}}, {{$subatAyrilanOgrenciErkek}}, {{$martAyrilanOgrenciErkek}}, {{$nisanAyrilanOgrenciErkek}}, {{$mayisAyrilanOgrenciErkek}}, {{$haziranAyrilanOgrenciErkek}}, {{$temmuzAyrilanOgrenciErkek}}, {{$agustosAyrilanOgrenciErkek}}, {{$eylulAyrilanOgrenciErkek}}, {{$ekimAyrilanOgrenciErkek}}, {{$kasimAyrilanOgrenciErkek}}, {{$aralikAyrilanOgrenciErkek}}]
                },
                {
                    label: "Bayan",
                    fillColor: "rgba(60,141,188,0.9)",
                    strokeColor: "rgba(60,141,188,0.8)",
                    pointColor: "#3b8bba",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: [{{$ocakAyrilanOgrenciKız}}, {{$subatAyrilanOgrenciKız}}, {{$martAyrilanOgrenciKız}}, {{$nisanAyrilanOgrenciKız}}, {{$mayisAyrilanOgrenciKız}}, {{$haziranAyrilanOgrenciKız}}, {{$temmuzAyrilanOgrenciKız}}, {{$agustosAyrilanOgrenciKız}}, {{$eylulAyrilanOgrenciKız}}, {{$ekimAyrilanOgrenciKız}}, {{$kasimAyrilanOgrenciKız}}, {{$aralikAyrilanOgrenciKız}}]
                }
            ]
        };
        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        @if(App\Helpers\helper::authControl('personel-istatistik','read'))
        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var say =0;
        var PieData = [
            <?php $say =0;
            $renk = ['#605CA8','#D2D6DE','#605CA8','#001F3F','#F39C12','#3C8DB3','#F56954','#56f556','#f5f556','#009933','#ff00bf','#ff0000','#993333'];
            ?>
            @foreach($teams as $value)
            {
                <?php
                $kont = App\User::where('delegation_id', $value->id)->count();
                ?>
                value: {{$kont}},
                color: "{{$renk[$say]}}",
                highlight: "{{$renk[$say]}}",
                label: "<?=$value->name?>"
            },
            <?php $say++;?>
            @endforeach
        ];
        @endif
        @if(App\Helpers\helper::authControl('ogrenci-istatistik','read'))
        var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
        var pieChart2 = new Chart(pieChartCanvas2);
        var PieData2 = [
            {
                value: {{$boy}},
                color: "#39CCCC",
                highlight: "#39CCCC",
                label: "Bay"
            },
            {
                value: {{$girl}},
                color: "#D81B60",
                highlight: "#D81B60",
                label: "Bayan"
            }
        ];
        @endif
        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%segments[i].fillColor%>\"></span><%if(segments[i].label){%><%segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        @if(App\Helpers\helper::authControl('personel-istatistik','read'))
        pieChart.Doughnut(PieData, pieOptions);
        @endif
        @if(App\Helpers\helper::authControl('ogrenci-istatistik','read'))
        pieChart2.Doughnut(PieData2, pieOptions);
        @endif

        //-------------
        //- BAR CHART -
        //-------------
        @if(App\Helpers\helper::authControl('ogrenci-konu-istatistik','read'))
        var barChartCanvas = $("#barChart").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[1].fillColor = "#DD4B39";
        barChartData.datasets[1].strokeColor = "#DD4B39";
        barChartData.datasets[1].pointColor = "#DD4B39";
        barChartData.datasets[0].fillColor = "#00C0EF";
        barChartData.datasets[0].strokeColor = "#00C0EF";
        barChartData.datasets[0].pointColor = "#00C0EF";
        barChartData.datasets[2].fillColor = "#00a65a";
        barChartData.datasets[2].strokeColor = "#00a65a";
        barChartData.datasets[2].pointColor = "#00a65a";
        @endif
        @if(App\Helpers\helper::authControl('kayit-yapilan-ogrenci-istatistik','read'))
        var barChartCanvas2 = $("#barChart2").get(0).getContext("2d");
        var barChart2 = new Chart(barChartCanvas2);
        var barChartData2 = areaChartData2;
        barChartData2.datasets[0].fillColor = "#39CCCC";
        barChartData2.datasets[0].strokeColor = "#39CCCC";
        barChartData2.datasets[0].pointColor = "#39CCCC";
        barChartData2.datasets[1].fillColor = "#D81B60";
        barChartData2.datasets[1].strokeColor = "#D81B60";
        barChartData2.datasets[1].pointColor = "#D81B60";
        @endif
        var barChartCanvas3 = $("#barChart3").get(0).getContext("2d");
        var barChart3 = new Chart(barChartCanvas3);
        var barChartData3 = areaChartData3;
        barChartData3.datasets[0].fillColor = "#39CCCC";
        barChartData3.datasets[0].strokeColor = "#39CCCC";
        barChartData3.datasets[0].pointColor = "#39CCCC";
        barChartData3.datasets[1].fillColor = "#D81B60";
        barChartData3.datasets[1].strokeColor = "#D81B60";
        barChartData3.datasets[1].pointColor = "#D81B60";

        var barChartOptions = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: true,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - If there is a stroke on each bar
          barShowStroke: true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth: 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing: 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing: 1,
          //String - A legend template
          legendTemplate: "<ul class=\"<%name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to make the chart responsive
          responsive: true,
          maintainAspectRatio: true,
          multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
        };

        barChartOptions.datasetFill = false;
        @if(App\Helpers\helper::authControl('ogrenci-konu-istatistik','read'))
        barChart.Bar(barChartData, barChartOptions);
        @endif
        @if(App\Helpers\helper::authControl('kayit-yapilan-ogrenci-istatistik','read'))
        barChart2.Bar(barChartData2, barChartOptions);
        @endif
        barChart3.Bar(barChartData3, barChartOptions);
    });

</script>
@endsection
