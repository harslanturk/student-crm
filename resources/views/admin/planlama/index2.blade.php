@extends('admin.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tüm Planlamalar
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/admin')}}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Planlama</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
             @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>{{Session::get('error')}}</strong>
                </div>
            @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>{{Session::get('success')}}</strong>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3">
                    <div>
                      @if(App\Helpers\helper::authControl('planlama','add'))
                      <a href="#" id="AddProgram" class="btn btn-block btn-primary btn-lg"  data-toggle="modal" data-target="#modalAddProgram" style="margin-bottom:20px;">Yeni Program Ekle</a>
                      @endif
                    </div>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Programlarım</h3>
                            <div class="box-tools">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                          <table class="table table-responsive">
                            @foreach($plans as $plan)
                              <!-- <li style="background-color:#F7F7F7;">
                                <b></b>
                              </li> -->
                              <?php
                                $renklendir = App\ProgramList::where('grup_no',$plan->id)->first();
                               ?>
                                 @if($renklendir)
                                   <tr>
                                     <td>
                                       {{$plan->hafta}}
                                     </td>
                                     <td>
                                       <a href="#" class="CopyModal" data-toggle="modal" data-target="#modalCopyModal" id="{{$plan->id}}" >
                                         <i class="fa fa-copy pull-right text-success"></i>
                                       </a>
                                       <a href="/admin/program/edit/{{$plan->id}}">
                                         <i class="fa fa-edit pull-right text-success"></i>
                                       </a>
                                     </td>
                                  </tr>
                                 @else
                                 <tr>
                                   <td colspan="2">
                                     <a href="/admin/program/show/{{$plan->id}}">{{$plan->hafta}}</a>
                                   </td>
                                 </tr>
                                 @endif
                              @endforeach
                          </table>
                        </div><!-- /.box-body -->
                    </div><!-- /. box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <!-- Randevu Ekleme Modeli -->
    <div class="modal fade" id="modalAddProgram" role="dialog">

    </div>
    <!-- /.modal -->
    <!-- Filtreleme Modeli -->
    <div class="modal fade" id="modalCopyModal" role="dialog">

    </div>
    <!-- /.modal -->
@endsection
@section('edit-js')
<script>
    $('#AddProgram').click(function () {
        var deger = 'a';
        /*console.log($(this).attr('name'));*/
        $.ajax({
            url: '/admin/planlama/createProgramModal',
            type: 'POST',
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            cache: false,
            data: {deger: deger},
            success: function(data){
                document.getElementById('modalAddProgram').innerHTML=data;
                //RenkSec(10,'color');
                $('.select2').select2();
            },
            error: function(jqXHR, textStatus, err){}
        });
    });
    $('.CopyModal').click(function () {
        var plan_id = $(this).attr('id');
        // console.log(plan_id);
        $.ajax({
            url: '/admin/program/CopyModal',
            type: 'POST',
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            cache: false,
            data: {plan_id: plan_id},
            success: function(data){
                document.getElementById('modalCopyModal').innerHTML=data;
                //RenkSec(10,'color');
                $('.select2').select2();
            },
            error: function(jqXHR, textStatus, err){}
        });
    });
</script>
@endsection
