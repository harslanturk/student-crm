<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/program/copySave')}}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$id}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Planlama Getir</h4>
            </div>
            <div class="modal-body">
              <div class="box-body">
                  <div class="col-sm-6" style="padding:0px;">
                      <label for="finish_date">Hafta Başlangıcı</label>
                      <input class="form-control" type="date" name="start">
                  </div>
                  <div class="col-sm-6" style="padding:0px;">
                      <label for="finish_date">Hafta Bitiş</label>
                      <input class="form-control" type="date" name="end">
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-primary">Planlama Kopyala</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
