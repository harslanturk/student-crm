@extends('admin.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tüm Planlamalar
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/admin')}}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Planlama</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box box-primary">
                      <form action="/admin/program/createProgram" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="grup_no" value="{{$id}}">
                        <table class="table table-responsive table-bordered">
                        <tr class="text-bold text-center">
                          <td colspan="3">Pazartesi</td>
                          <td colspan="3">Salı</td>
                          <td colspan="3">Çarşamba</td>
                          <td colspan="3">Perşembe</td>
                          <td colspan="3">Cuma</td>
                        </tr>
                        <?php $grup_no = 1;
                        $student = '<option value="0" selected>Öğrenci Seçiniz.</option>';
                        foreach($ogrencis as $key => $ogrenci){
                          $student .='<option value="'.$ogrenci->id.'">'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
                        }
                        $otobus = '<option value="0" selected>Servis Seçiniz.</option>';
                        foreach($servis as $key => $servi){
                          $otobus .='<option value="'.$servi->id.'">'.$servi->name.'</option>';
                        }
                         ?>
                         <input type="hidden" name="grup_count" value="{{count($grups)}}">
                         <input type="hidden" name="sinif_count" value="{{count($sinifs)}}">
                        @foreach($grups as $key => $grup)
                        <tr class="text-bold text-center" style="background-color:lightgreen;">
                          <td colspan="3">{{$grup->name}}</td>
                          <td colspan="6">
                            {{$grup_no}}.Seans
                            {{$grup->seans_one_start}} -  {{$grup->seans_one_end}}  /  {{$grup->mola}} dk Mola
                          </td>
                          <td colspan="6">
                          <?php $grup_no++; ?>
                            {{$grup_no}}.Seans
                            {{$grup->seans_two_start}} - {{$grup->seans_two_end}}  /  {{$grup->mola}} dk Mola
                          </td>
                        </tr>
                        <?php $grup_no++; ?>
                        <tr>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                        </tr>
                        <?php $say_bakalim = 0; ?>
                        @foreach($sinifs as $anahtar => $sinif)
                        <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/pazartesi">
                        <tr>
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <?php
                                  /*echo $plan[$anahtar][0]->ogrenci_id."---";
                                  echo "<pre>";
                                  print_r($plan[$anahtar][0]);
                                  die();*/
                                  /*$program = App\Helpers\helper::programEdit($sinif->name,'pazartesi','a',$id,($key+1));*/
                                  //$program = $plan;

                                ?>
                                <td>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        <?php $deger = $plan[$anahtar][0]->ogrenci_id; ?>
                                        @if($deger == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as  $servi)
                                      @if($plan[$anahtar][0]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach                        
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'pazartesi','b',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][1]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][1]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/sali">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'sali','a',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][2]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][2]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'sali','b',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][3]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][3]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/carsamba">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'carsamba','a',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][4]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][4]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'carsamba','b',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][5]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][5]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/persembe">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'persembe','a',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][6]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][6]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'persembe','b',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][7]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][7]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/cuma">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'cuma','a',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][8]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][8]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php
                                    //$program = App\Helpers\helper::programEdit($sinif->name,'cuma','b',$id,($key+1));
                                  ?>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                        <option value="0">Öğrenci Seçiniz.</option>
                                      @foreach($ogrencis as $ogrenci)
                                        @if($plan[$anahtar][9]->ogrenci_id == $ogrenci->id)
                                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @else
                                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                                        @endif
                                      @endforeach
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                      <option value="0">Servis Seçiniz.</option>
                                    @foreach($servis as $servi)
                                      @if($plan[$anahtar][9]->servis_id == $servi->id)
                                      <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                                      @else
                                      <option value="{{$servi->id}}">{{$servi->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <?php $say_bakalim++; ?>
                        @endforeach
                        @endforeach
                      </table>
                      <div class="form-group">
                        <button class="btn btn-success" type="submit" name="button">Güncelle</button>
                      </div>
                      </form>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection
@section('edit-js')
<script>
setTimeout(function(){
  $('.sidebar-toggle').click(),1000
});
</script>
@endsection
