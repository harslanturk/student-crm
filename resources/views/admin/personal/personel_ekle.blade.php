@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Personel Ekle
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Personel Ekle</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Personel Ekle</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <div role="form">
          <div class="box-body">
              <div class="form-group col-md-4">
                  <label for="inputEmail">E-Mail</label>
                  <input type="text" class="form-control" id="inputEmail" placeholder="E-Mail">
              </div>
              <div class="form-group col-md-4">
                  <label for="inputPass">Şifre</label>
                  <input type="password" class="form-control" id="inputPass" placeholder="Şifre">
              </div>
            <div class="form-group col-md-4">
                <label for="inputYetkiGrubu">Yetki Gurubu</label>
                <select class="form-control" id="inputYetkiGrubu">
                    @foreach($userDelegation as $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inputTcNo">TC No</label>
                <input type="text" class="form-control" id="inputTcNo" placeholder="TC No">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdi">Ad Soyad</label>
                <input type="text" class="form-control" id="inputName" placeholder="Ad Soyad">
            </div>
            <div class="form-group col-md-4">
                <label for="inputCinsiyeti">Cinsiyeti</label>
                <input type="text" class="form-control" id="inputCinsiyeti" placeholder="Cinsiyeti">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDogumTarihi">Doğum Tarihi</label>
                <input type="date" class="form-control" id="inputDogumTarihi" placeholder="Doğum Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDogumYeri">Doğum Yeri</label>
                <input type="text" class="form-control" id="inputDogumYeri" placeholder="Doğum Yeri">
            </div>
            <div class="form-group col-md-4">
                <label for="inputBabaAdi">Baba Adı Soyadı</label>
                <input type="text" class="form-control" id="inputBabaAdi" placeholder="Baba Adı Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAnneAdi">Anne Adı Soyadı</label>
                <input type="text" class="form-control" id="inputAnneAdi" placeholder="Anne Adı Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputKayitTarihi">Kayıt Tarihi</label>
                <input type="date" class="form-control" id="inputKayitTarihi" placeholder="Kayıt Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAyrilisTarihi">Ayrılış Tarihi</label>
                <input type="date" class="form-control" id="inputAyrilisTarihi" placeholder="Ayrılış Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputTelefon">Telefon</label>
                <input type="text" class="form-control" id="inputTelefon" placeholder="Diğer Telefon">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIsTelefon">İş Telefon</label>
                <input type="text" class="form-control" id="inputIsTelefon" placeholder="İş Telefon">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDigerTelefon">Diğer Telefon</label>
                <input type="text" class="form-control" id="inputDigerTelefon" placeholder="Diğer Telefon">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIl">İl</label>
                <input type="text" class="form-control" id="inputIl" placeholder="İl">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIlce">İlçe</label>
                <input type="text" class="form-control" id="inputIlce" placeholder="İlçe">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Adres</label>
                <input type="text" class="form-control" id="inputAdres" placeholder="Adres">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Planlama Renk</label>
                <input type="text" class="form-control my-colorpicker1" id="inputColor" placeholder="Planlama Rengi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Sınıf Seç</label>
                <select class="form-control select2" name="sinif_id" id="sinif_id">
                  @foreach($sinif as $val)
                  <option value="{{$val->id}}">{{$val->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Servis Seç</label>
                <select class="form-control select2" name="servis_id" id="servis_id">
                  @foreach($servis  as $val)
                  <option value="{{$val->id}}">{{$val->name}}</option>
                  @endforeach
                </select>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" onclick="addPersonel()">Kaydet</button>
          </div>
        </div>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()
