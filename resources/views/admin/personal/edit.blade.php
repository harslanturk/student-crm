@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Personel Düzenle
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Personel Düzenle</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="
                @if(isset($personelResim))
                {{ $personelResim->resim }}
                @else
                /img/user.png
                @endif
                        " alt="User profile picture">
                <h3 class="profile-username text-center">{{ $user->ad }} {{ $user->soyad }}</h3>
                <!--<p class="text-muted text-center">Software Engineer</p>-->

                @if(App\Helpers\helper::authControl('personel-fotograf','add'))
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <form action="/admin/staff/update-resim" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputFile"><b>Profil Resmi Değiştir:</b></label>
                            <input type="file" id="profilResim" name="resim">
                            <input type="hidden" value="{{ $user->id }}" name="personel_id" id="presim_personel_id">
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" >Resim Yükle</button><!--onclick="saveStudentImage()"-->
                        </div>
                        </form>
                    </li>
                </ul>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>   <!-- /.row -->
    <div class="col-md-9">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Personel Düzenle</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form action="/admin/staff/update" method="post">
          {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div role="form">
          <div class="box-body">
              <div class="form-group col-md-4">
                  <label for="inputEmail">E-Mail</label>
                  <input type="text" class="form-control" id="inputEmail" name="email" value="{{ $user->email }}" placeholder="E-Mail">
              </div>
              <div class="form-group col-md-4">
                  <label for="inputPass">Şifre</label>
                  <input type="password" class="form-control" id="inputPass" name="password" placeholder="Şifre">
              </div>
            <div class="form-group col-md-4">
                <label for="inputYetkiGrubu">Yetki Gurubu</label>
                <select class="form-control" id="inputYetkiGrubu" name="delegation_id">
                    @foreach($userDelegation as $value)
                        @if($value->id==$user->delegation_id)
                        <option selected value="{{ $value->id }}">{{ $value->name }}</option>
                        @else
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inputTcNo">TC No</label>
                <input type="text" class="form-control" id="inputTcNo" name="tcno" value="{{ $user->tcno }}" placeholder="TC No">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdi">Ad Soyad</label>
                <input type="text" class="form-control" id="inputName" name="name" value="{{ $user->name }}" placeholder="Ad Soyad">
            </div>
            <div class="form-group col-md-4">
                <label for="inputCinsiyeti">Cinsiyeti</label>
                <input type="text" class="form-control" id="inputCinsiyeti" name="cinsiyet" value="{{ $user->cinsiyet }}" placeholder="Cinsiyeti">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDogumTarihi">Doğum Tarihi</label>
                <input type="date" class="form-control" id="inputDogumTarihi" name="dogum_tarihi" value="{{ $user->dogum_tarihi }}" placeholder="Doğum Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDogumYeri">Doğum Yeri</label>
                <input type="text" class="form-control" id="inputDogumYeri" name="dogum_yeri" value="{{ $user->dogum_yeri }}" placeholder="Doğum Yeri">
            </div>
            <div class="form-group col-md-4">
                <label for="inputBabaAdi">Baba Adı Soyadı</label>
                <input type="text" class="form-control" id="inputBabaAdi" name="baba" value="{{ $user->baba }}" placeholder="Baba Adı Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAnneAdi">Anne Adı Soyadı</label>
                <input type="text" class="form-control" id="inputAnneAdi" name="anne" value="{{ $user->anne }}" placeholder="Anne Adı Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputKayitTarihi">Kayıt Tarihi</label>
                <input type="date" class="form-control" id="inputKayitTarihi" name="kayit_tarihi" value="{{ $user->kayit_tarihi }}" placeholder="Kayıt Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAyrilisTarihi">Ayrılış Tarihi</label>
                <input type="date" class="form-control" id="inputAyrilisTarihi" name="ayrilis_tarihi" value="{{ $user->ayrilis_tarihi }}" placeholder="Ayrılış Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputTelefon">Telefon</label>
                <input type="text" class="form-control" id="inputTelefon" name="telefon" value="{{ $user->telefon }}" placeholder="Diğer Telefon">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIsTelefon">İş Telefon</label>
                <input type="text" class="form-control" id="inputIsTelefon" name="is_telefon" value="{{ $user->is_telefon }}" placeholder="İş Telefon">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDigerTelefon">Diğer Telefon</label>
                <input type="text" class="form-control" id="inputDigerTelefon" name="diger_telefon" value="{{ $user->diger_telefon }}" placeholder="Diğer Telefon">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIl">İl</label>
                <input type="text" class="form-control" id="inputIl" name="il" value="{{ $user->il }}" placeholder="İl">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIlce">İlçe</label>
                <input type="text" class="form-control" id="inputIlce" name="ilce" value="{{ $user->ilce }}" placeholder="İlçe">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Adres</label>
                <input type="text" class="form-control" id="inputAdres" name="adres" value="{{ $user->adres }}" placeholder="Adres">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Planlama Renk</label>
                <input type="text" class="form-control my-colorpicker1" id="inputColor" name="color" value="{{ $user->color }}" placeholder="Planlama Rengi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Sınıf Seç</label>
                <select class="form-control select2" name="sinif_id">
                  @foreach($sinif as $val)
                  @if($user->sinif_id == $val->id)
                  <option value="{{$val->id}}" selected>{{$val->name}}</option>
                  @else
                  <option value="{{$val->id}}">{{$val->name}}</option>
                  @endif
                  @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Servis Seç</label>
                <select class="form-control select2" name="servis_id">
                  @foreach($servis  as $val)
                  @if($user->servis_id == $val->id)
                  <option value="{{$val->id}}" selected>{{$val->name}}</option>
                  @else
                  <option value="{{$val->id}}">{{$val->name}}</option>
                  @endif
                  @endforeach
                </select>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Kaydet</button>
          </div>
        </div>
        </form>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()
