@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Tüm Personeller
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Personel Listesi</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
      <div class="box-header">
        <h3 class="box-title">
          @if(App\Helpers\helper::authControl('personel-ekle','add'))
          <a href="/admin/staff/create"><button class="btn btn-primary pull-right" style=""><b>Yeni Personel</b></button></a>
          @endif
        </h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="ogrenci-table" class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th>Ad Soyad</th>
              <th>E-Posta</th>
              <th>Telefon</th>
              <th>Yetki</th>
              <th>İşlemler</th>
            </tr>
          </thead>
          <tbody>
          @foreach($personel as $person)
            <tr><!--  onclick="document.location = '/admin/staff/edit/{{ $person->id }}';" style="cursor: pointer;"-->
              <td>{{ $person->name }}</td>
              <td>{{ $person->email }}</td>
              <td>{{ $person->telefon }}</td>
              <td>{{ $person->delegName }}</td>
              <td>
                @if(App\Helpers\helper::authControl('tum-personeller','update'))
                  <a href="/admin/staff/edit/{{$person->id}}" class="button btn btn-success"><i class="fa fa-edit"> Düzenle</i></a>
                @endif
                @if(App\Helpers\helper::authControl('tum-personeller','delete'))
                  <a class="button btn btn-danger" onclick="deleteApprove('/admin/staff/delete/{{ $person->id }}')"><i class="fa fa-trash"> Sil</i></a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Ad Soyad</th>
              <th>TC No</th>
              <th>Telefon</th>
              <th>Yetki</th>
              <th>İşlemler</th>
            </tr>
          </tfoot>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()
