@extends('admin.master')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tanımlamalar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li class="active">Tanımlamalar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tanımlamalar</h3>
              <div class="box-tools">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/ogrenci-islemleri"><i class="fa fa-inbox"></i> Öğrenci İşlemleri</a></li>
                <li><a href="/engel-tipleri"><i class="fa fa-envelope-o"></i> Engel Tipleri</a></li>
                <li><a href="/student-visit-status"><i class="fa fa-envelope-o"></i> Aile Görüşme Durumu</a></li>
                <li><a href="/sinif"><i class="fa fa-envelope-o"></i> Sınıf</a></li>
                <li><a href="/servis"><i class="fa fa-envelope-o"></i> Servis</a></li>
                <li class="active"><a href="/plan-group"><i class="fa fa-envelope-o"></i> Grup</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->
        </div><!-- /.col -->
        <div class="col-md-9">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Grup Tanımlamaları</h3>
              </div><!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  @if(App\Helpers\helper::authControl('tanimlamalar','add'))
                  <tr>
                    <td colspan="3">
                        <table style="width:100%;">
                          <tr>
                            <td><b>Yeni Grup Ekle</b></td>
                            <td class="pull-right">
                              <a href="#" class="GrupEkle btn btn-primary" style="margin:0 0 0 3px;" data-toggle="modal" data-target="#modalgrupEkle">Ekle
                              </a>
                            </td>
                          </tr>
                        </table>
                    </td>
                  </tr>
                  @endif
                  <tr>
                    <th>ID</th>
                    <th>İşlem Adı</th>
                    <th>#</th>
                  </tr>
                  @foreach($planGroup as $kont)
                  <tr>
                    <td>{{ $kont->id }}</td>
                    <td>{{ $kont->name }}</td>
                    <td>
                      @if(App\Helpers\helper::authControl('tanimlamalar','delete'))
                        <a href="/sil/grup/{{ $kont->id }}" onclick="return silOnayla();">
                          <i class="glyphicon glyphicon-remove" style="color:red;"></i>
                        </a>
                      @endif
                      @if(App\Helpers\helper::authControl('tanimlamalar','update'))
                        <a href="#" class="GrupGuncelle" id="{{ $kont->id }}" style="margin:0 0 0 3px;" data-toggle="modal" data-target="#modalgrupGuncelle">
                          <i class="fa fa-external-link text-green"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <!-- EngelTipGuncelle Modal -->
  <div class="modal fade" id="modalgrupEkle" tabindex="-1" role="dialog">

  </div><!-- /.modal -->
  <div class="modal fade" id="modalgrupGuncelle" tabindex="-1" role="dialog">

  </div><!-- /.modal -->
  <script type="text/javascript">
      $(document).ready(function () {
        $('.GrupEkle').click(function () {
            var a = ' ';
            //console.log(engel_id);
            $.ajax({
                url: '/planGrupEkleModal',
                type: 'POST',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                cache: false,
                data: {a: a},
                success: function(data){
                    document.getElementById('modalgrupEkle').innerHTML=data;
                },
                error: function(jqXHR, textStatus, err){}
            });
        });
          $('.GrupGuncelle').click(function () {
              var grup_id = $(this).attr('id');
              //console.log(engel_id);
              $.ajax({
                  url: '/planGrupEdit',
                  type: 'POST',
                  beforeSend: function (xhr) {
                      var token = $('meta[name="csrf_token"]').attr('content');

                      if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                      }
                  },
                  cache: false,
                  data: {grup_id: grup_id},
                  success: function(data){
                      document.getElementById('modalgrupGuncelle').innerHTML=data;
                  },
                  error: function(jqXHR, textStatus, err){}
              });
          });
      });
  </script>
@stop()
