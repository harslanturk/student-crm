<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/planGrupEkle')}}" method="POST">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Grup Ekle</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputPersonel">Grup Adı</label>
                        <input class="form-control" type="text" name="name">
                    </div>
                    <div class="form-group col-sm-6" style="padding:0px;">
                        <label for="exampleInputPersonel">Seans 1 Başlangıcı</label>
                        <input class="form-control" type="time" name="seans_one_start">
                    </div>
                    <div class="form-group col-sm-6" style="padding:0px;">
                        <label for="exampleInputPersonel">Seans 1 Bitiş</label>
                        <input class="form-control" type="time" name="seans_one_end">
                    </div>
                    <div class="form-group col-sm-6" style="padding:0px;">
                        <label for="exampleInputPersonel">Seans 2 Başlangıcı</label>
                        <input class="form-control" type="time" name="seans_two_start">
                    </div>
                    <div class="form-group col-sm-6" style="padding:0px;">
                        <label for="exampleInputPersonel">Seans 2 Bitiş</label>
                        <input class="form-control" type="time" name="seans_two_end">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPersonel">Mola</label>
                        <input class="form-control" type="text" name="mola">
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
