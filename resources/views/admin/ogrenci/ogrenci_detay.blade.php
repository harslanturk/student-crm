@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Öğrenci Detay
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Öğrenci Detay</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="@if(isset($ogrenciResim))
                          {{ $ogrenciResim->resim }}
                          @else
                          /img/user.png
                          @endif" alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->

                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Öğrenci No:</b> <a class="pull-right">{{ $ogrenci->ogrenci_no }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>T.C. Kimlik No:</b> <a class="pull-right">{{ $ogrenci->tcno }}</a>
                              </li>
                          </ul>
                          @if(App\Helpers\helper::authControl('ogrenci-detay','read'))
                          <a href="/admin/students/edit/{{ $ogrenci->id }}" class="btn btn-primary btn-block none-print"><b>Detaylar</b></a>
                          @endif
                          <a href="/admin/students/family-show/{{ $ogrenci->id }}" class="btn btn-success btn-block none-print"><b>Aile Görüşmesi</b></a>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <!-- general form elements -->
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Son Açılan 10 Konu</h3>
                          @if(App\Helpers\helper::authControl('ogrenci-yazdir','read'))
                          <span class="pull-right none-print">
                              <div class="btn-group-vertical">
                                  <button type="button" class="btn btn-success" onClick="window.print()">
                                      <i class="fa fa-print"></i>
                                      Yazdır
                                  </button>
                              </div>
                          </span>
                          @endif
                          @if(App\Helpers\helper::authControl('ogrenci-yeni-konu','add'))
                          <button class="btn btn-primary pull-right none-print" data-toggle="modal" data-target="#modalYeniKonu" style="width: 20%; margin-right:20px;"><b>Yeni Konu</b></button>
                          @endif
                      </div><!-- /.box-header -->
                      <div class="box-body table-responsive no-padding">
                          <table class="table table-striped">
                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>Tarih</th>
                                  <th>Konu</th>
                                  <th>Görevlendirilen</th>
                                  <th>İşlem</th>
                                  <th>Durum</th>
                                  <th>Açıklama</th>
                              </tr>
                              <?php $sayac=1; ?>
                              @foreach($islemDetay as $key => $value)
                              <tr onclick="document.location = '/admin/process/show/{{ $value->id }}';" style="cursor: pointer;">
                                  <td>{{ $sayac }}</td>
                                  <td><?php $newdate = App\Helpers\Helper::DateConverter($value->created_at); echo $newdate; ?></td>
                                  <td>{{ $value->konu_islem }}</td>
                                  <td>{{ $value->sorumlu }}</td>
                                  <td>{{ $value->islem }}</td>
                                  @if($value->status==1)
                                  <td style="color:green;">Açık</td>
                                  @else
                                  <td style="color:red;">Kapalı</td>
                                  @endif
                                  <td>{{ $value->aciklama }}</td>
                              </tr>
                              <?php $sayac++; ?>
                              @endforeach

                          </table>
                      </div><!-- /.box -->
                  </div><!-- /.box -->
              </div><!--/.col (left) -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="modalYeniKonu" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="/admin/process/saveKonu" method="POST">
            {{ csrf_field() }}
        <input type="hidden" name="ogrenci_id" value="{{ $ogrenci->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Yeni Konu</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputKonu">Konu</label>
                            <select class="form-control select2" style="width:100%;" id="inputKonu" name="konu_id">
                                <option >Lütfen Seçiniz</option>
                                <?php $kontrol=0; ?>
                                @foreach($allogrenciIslemleri as $value)
                                    @foreach($islemDetaySp as $spvalue)
                                        @if($spvalue->konu_islem == $value->islem)
                                            <?php $kontrol =1; ?>
                                        @endif
                                    @endforeach
                                    @if($kontrol==0)
                                            <option value="{{ $value->id }}">{{ $value->islem }}</option>
                                    @endif
                                    <?php $kontrol=0; ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Görevlendirilen Kişi</label>
                            <select class="form-control select2" style="width:100%;" id="inputKonuPersonel" name="alici_id">
                                @foreach($allpersonel as $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputIslem">İşlem</label>
                            <select class="form-control" name="islem" id="inputKonuIslem">
                                <option>Yeni İşlem</option>
                                <!--<option>İşlem Yapılıyor</option>
                                <option>Onaylandı</option>
                                <option>Red</option>-->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Açıklama</label>
                            <textarea class="form-control" rows="3" placeholder="Açıklama" name="aciklama" maxlength="160" onkeyup="islem_gonder_elle()" id="islem_mesaj_secerek"></textarea>
                        </div>
                        <div class="form-group" style="text-align:right">
                            <label class="control-label">Kalan Karakter :<span class="control-label" id="islem_kalan_karakter">160</span></label>

                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop()
