@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- ALERT -->
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
    @endif
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ayrılan Öğrenciler
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Öğrenci Listesi</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a href="{{URL::to('/admin/students/list')}}" class="btn btn-success text-bold">Mevcut Öğrenciler</a>
                            </div>
                            @if(App\Helpers\helper::authControl('ogrenci-ekle','add'))
                            <div class="pull-right">
                                <a href="{{URL::to('/admin/students/create')}}" class="btn btn-primary text-bold">Yeni Öğrenci</a>
                            </div>
                            @endif
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="ogrenci-table" class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Sıra</th>
                                    <th>Öğrenci No</th>
                                    <th>Adı</th>
                                    <th>Soyadı</th>
                                    <th>TC No</th>
                                    <th>Cinsiyet</th>
                                    <th>Engel Tipi</th>
                                    <th>İşlemler</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $sira=1; ?>
                                @foreach($ogrenci as $student)
                                    <tr><!-- onclick="document.location = '/admin/students/show/{{ $student->id }}';" style="cursor: pointer;" -->
                                        <td>{{ $sira }}</td>
                                        <td><a href="/admin/students/show/{{$student->id}}">{{ $student->ogrenci_no }}</a></td>
                                        <td><a href="/admin/students/show/{{$student->id}}">{{ $student->ad }}</a></td>
                                        <td><a href="/admin/students/show/{{$student->id}}">{{ $student->soyad }}</a></td>
                                        <td><a href="/admin/students/show/{{$student->id}}">{{ $student->tcno }}</a></td>
                                        <td> <a href="/admin/students/show/{{$student->id}}">{{ $student->cinsiyet }}</a></td>
                                        <td><a href="/admin/students/show/{{$student->id}}">{{ $student->engel_tipi }}</a></td>
                                        <td>
                                          @if(App\Helpers\helper::authControl('tum-ogrenciler','delete'))
                                          <a class="button btn btn-danger" onclick="deleteApprove('/admin/students/delete/{{ $student->id }}')"><i class="fa fa-trash"> Sil</i></a>
                                          @endif
                                        </td>
                                    </tr>
                                    <?php $sira++; ?>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Sıra</th>
                                    <th>Öğrenci No</th>
                                    <th>Adı</th>
                                    <th>Soyadı</th>
                                    <th>TC No</th>
                                    <th>Cinsiyet</th>
                                    <th>Engel Tipi</th>
                                    <th>İşlemler</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@stop()
