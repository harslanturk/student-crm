@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Öğrenci Ekle
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Öğrenci Ekle</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Öğenci Ekle</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
      <form action="/admin/students/new" method="POST">
      {{ csrf_field() }}
        <div role="form">
          <div class="box-body">
            <div class="form-group col-md-4">
                <label for="inputOgrenciNo">Öğrenci No</label>
                <input type="text" class="form-control" name="inputOgrenciNo" placeholder="Öğrenci Numarası">
            </div>
            <div class="form-group col-md-4">
                <label for="inputTcNo">TC No</label>
                <input type="text" class="form-control" name="inputTcNo" placeholder="TC No">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEngelTipi">Engel Tipi</label>
              <select class="form-control" name="inputEngelTipi">
                  <option disabled>Lütfen Seçiniz</option>
                  @foreach($engelTipleri as $value)
                          <option value="{{ $value->id }}">{{ $value->engel }}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdi">Adı</label>
                <input type="text" class="form-control" name="inputAdi" placeholder="Adı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputSoyadi">Soyadı</label>
                <input type="text" class="form-control" name="inputSoyadi" placeholder="Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputCinsiyeti">Cinsiyeti</label>
                <input type="text" class="form-control" name="inputCinsiyeti" placeholder="Cinsiyeti">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDogumTarihi">Doğum Tarihi</label>
                <input type="date" class="form-control" name="inputDogumTarihi" placeholder="Doğum Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDogumYeri">Doğum Yeri</label>
                <input type="text" class="form-control" name="inputDogumYeri" placeholder="Doğum Yeri">
            </div>
            <div class="form-group col-md-4">
                <label for="inputOkulunAdi">Okulun Adı</label>
                <input type="text" class="form-control" name="inputOkulunAdi" placeholder="Okulun Adı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputBabaAdi">Baba Adı Soyadı</label>
                <input type="text" class="form-control" name="inputBabaAdi" placeholder="Baba Adı Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputBabaTel">Baba Tel</label>
                <input type="text" class="form-control" name="inputBabaTel" placeholder="Baba Tel">
            </div>
            <div class="form-group col-md-4">
                <label for="inputKayitTarihi">Kayıt Tarihi</label>
                <input type="date" class="form-control" name="inputKayitTarihi" placeholder="Kayıt Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAnneAdi">Anne Adı Soyadı</label>
                <input type="text" class="form-control" name="inputAnneAdi" placeholder="Anne Adı Soyadı">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAnneTel">Anne Tel</label>
                <input type="text" class="form-control" name="inputAnneTel" placeholder="Anne Tel">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAyrilisTarihi">Ayrılış Tarihi</label>
                <input type="date" class="form-control" name="inputAyrilisTarihi" placeholder="Ayrılış Tarihi">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEgitimSekli">Eğitim Şekli</label>
                <input type="text" class="form-control" name="inputEgitimSekli" placeholder="Eğitim Şekli">
            </div>
            <div class="form-group col-md-4">
                <label for="inputDigerTel">Diğer Tel</label>
                <input type="text" class="form-control" name="inputDigerTel" placeholder="Diğer Tel">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIl">İl</label>
                <input type="text" class="form-control" name="inputIl" placeholder="İl">
            </div>
            <div class="form-group col-md-4">
                <label for="inputIlce">İlçe</label>
                <input type="text" class="form-control" name="inputIlce" placeholder="İlçe">
            </div>
            <div class="form-group col-md-4">
                <label for="inputAdres">Adres</label>
                <input type="text" class="form-control" name="inputAdres" placeholder="Adres">
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Kaydet</button>
          </div>
        </div>
      </form>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()