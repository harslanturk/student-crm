@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Öğrenci Detay
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Öğrenci Detay</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="
                          @if(isset($ogrenciResim))
                          {{ $ogrenciResim->resim }}
                          @else
                          /img/user.png
                          @endif
                                  " alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->
                          @if(App\Helpers\helper::authControl('ogrenci-fotograf','add'))
                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <form action="/admin/students/update-resim" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <div class="form-group">
                                      <label for="exampleInputFile"><b>Profil Resmi Değiştir:</b></label>
                                      <input type="file" id="profilResim" name="resim">
                                      <input type="hidden" value="{{ $ogrenci->id }}" name="ogrenci_id" id="presim_ogrenci_id">
                                  </div>
                                  <div>
                                      <button type="submit" class="btn btn-primary" >Resim Yükle</button><!--onclick="saveStudentImage()"-->
                                  </div>
                                  </form>
                              </li>
                          </ul>
                          @endif
                          <a href="/admin/students/show/{{ $ogrenci->id }}" class="btn btn-primary btn-block"><b>İşlemler</b></a>
                          @if($ogrenci->ayrilis_tarihi == "0000-00-00")
                          <a href="/admin/students/leave/{{ $ogrenci->id }}" class="btn btn-danger btn-block"><b>Öğrenciyi Ayrılt</b></a>
                          @else
                          <a href="/admin/students/active/{{ $ogrenci->id }}" class="btn btn-success btn-block"><b>Öğrenciyi Ayrıltma</b></a>
                          @endif
                          @if(App\Helpers\helper::authControl('ogrenciler','delete'))
                          <a href="#" onclick="deleteApproveStudent('/admin/students/delete/{{ $ogrenci->id }}',{{$islem_detay}})" class="btn btn-danger btn-block none-print"><b>Öğrenciyi Sil</b></a>
                          @endif
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <!-- general form elements -->
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title"></h3>
                      </div><!-- /.box-header -->
                      <div role="form">
                          <div class="box-body">
                              <div class="form-group col-md-4">
                                  <label for="inputOgrenciNo">Öğrenci No</label>
                                  <input type="hidden" id="inputOgrenciId" value="{{ $ogrenci->id }}">
                                  <input type="number" class="form-control" id="inputOgrenciNo" placeholder="Öğrenci Numarası" value="{{$ogrenci->ogrenci_no}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputTcNo">TC No</label>
                                  <input type="number" class="form-control" id="inputTcNo" placeholder="TC No" value="{{$ogrenci->tcno}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputEngelTipi">Engel Tipi</label>
                                  <select class="form-control" id="inputEngelTipi">
                                      <option disabled>Lütfen Seçiniz</option>
                                      @foreach($engelTipleri as $value)
                                          @if($ogrenci->engel_tipi==$value->id)
                                          <option selected value="{{ $value->id }}">{{ $value->engel }}</option>
                                          @else
                                          <option value="{{ $value->id }}">{{ $value->engel }}</option>
                                          @endif
                                      @endforeach
                                  </select>
                                  <!--<input type="text" class="form-control" id="inputEngelTipi" placeholder="Engel Tipi" value="{{$ogrenci->engel_tipi}}">-->
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputAdi">Adı</label>
                                  <input type="text" class="form-control" id="inputAdi" placeholder="Adı" value="{{$ogrenci->ad}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputSoyadi">Soyadı</label>
                                  <input type="text" class="form-control" id="inputSoyadi" placeholder="Soyadı" value="{{$ogrenci->soyad}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputCinsiyeti">Cinsiyeti</label>
                                  <select class="form-control" id="inputCinsiyeti">
                                  <option disabled>Lütfen Seçiniz</option>
                                  <option @if($ogrenci->cinsiyet == "Erkek") selected @endif>Erkek</option>
                                  <option @if($ogrenci->cinsiyet == "Kız") selected @endif>Kız</option>
                                  </select>
                                  <!--<input type="text" class="form-control" id="inputCinsiyeti" placeholder="Cinsiyeti" value="{{$ogrenci->cinsiyet}}">-->
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputDogumTarihi">Doğum Tarihi</label>
                                  <input type="date" class="form-control" id="inputDogumTarihi" placeholder="Doğum Tarihi" value="{{$ogrenci->dogum_tarihi}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputDogumYeri">Doğum Yeri</label>
                                  <input type="text" class="form-control" id="inputDogumYeri" placeholder="Doğum Yeri" value="{{$ogrenci->dogum_yeri}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputOkulunAdi">Okulun Adı</label>
                                  <input type="text" class="form-control" id="inputOkulunAdi" placeholder="Okulun Adı" value="{{$ogrenci->dogum_okul}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputBabaAdi">Baba Adı Soyadı</label>
                                  <input type="text" class="form-control" id="inputBabaAdi" placeholder="Baba Adı Soyadı" value="{{$ogrenci->baba}}">
                              </div>
                              <!--<div class="form-group col-md-4">
                                  <label for="inputBabaTel">Baba Tel</label>
                                  <input type="text" class="form-control" id="inputBabaTel" placeholder="Baba Tel" value="{{$ogrenci->baba_tel}}">
                              </div>-->
                              <div class="form-group col-md-4">
                                  <label  for="inputBabaTel">Baba Tel</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="text" class="form-control" id="inputBabaTel" value="{{$ogrenci->baba_tel}}" data-inputmask='"mask": "(999) 999 99 99"' data-mask>
                                  </div><!-- /.input group -->
                              </div><!-- /.form group -->
                              <div class="form-group col-md-4">
                                  <label for="inputKayitTarihi">Kayıt Tarihi</label>
                                  <input type="date" class="form-control" id="inputKayitTarihi" placeholder="Kayıt Tarihi" value="{{$ogrenci->kayit_tarihi}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputAnneAdi">Anne Adı Soyadı</label>
                                  <input type="text" class="form-control" id="inputAnneAdi" placeholder="Anne Adı Soyadı" value="{{$ogrenci->anne}}">
                              </div>
                              <!--<div class="form-group col-md-4">
                                  <label for="inputAnneTel">Anne Tel</label>
                                  <input type="text" class="form-control" id="inputAnneTel" placeholder="Anne Tel" value="{{$ogrenci->anne_tel}}">
                              </div>-->
                              <div class="form-group col-md-4">
                                  <label  for="inputAnneTel">Anne Tel</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="text" class="form-control" id="inputAnneTel" value="{{$ogrenci->anne_tel}}" data-inputmask='"mask": "(999) 999 99 99"' data-mask>
                                  </div><!-- /.input group -->
                              </div><!-- /.form group -->
                              <div class="form-group col-md-4">
                                  <label for="inputAyrilisTarihi">Ayrılış Tarihi</label>
                                  <input type="date" class="form-control" id="inputAyrilisTarihi" placeholder="Ayrılış Tarihi" value="{{$ogrenci->ayrilis_tarihi}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputEgitimSekli">Eğitim Şekli</label>
                                  <input type="text" class="form-control" id="inputEgitimSekli" placeholder="Eğitim Şekli" value="{{$ogrenci->egitim}}">
                              </div>
                              <!--<div class="form-group col-md-4">
                                  <label for="inputDigerTel">Diğer Tel</label>
                                  <input type="text" class="form-control" id="inputDigerTel" placeholder="Diğer Tel" value="{{$ogrenci->diger_tel}}">
                              </div>-->
                              <div class="form-group col-md-4">
                                  <label  for="inputDigerTel">Diğer Tel</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="text" class="form-control" id="inputDigerTel" value="{{$ogrenci->diger_tel}}" data-inputmask='"mask": "(999) 999 99 99"' data-mask>
                                  </div><!-- /.input group -->
                              </div><!-- /.form group -->
                              <div class="form-group col-md-4">
                                  <label for="inputIl">İl</label>
                                  <input type="text" class="form-control" id="inputIl" placeholder="İl" value="{{$ogrenci->il}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputIlce">İlçe</label>
                                  <input type="text" class="form-control" id="inputIlce" placeholder="İlçe" value="{{$ogrenci->ilce}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputAdres">Adres</label>
                                  <input type="text" class="form-control" id="inputAdres" placeholder="Adres" value="{{$ogrenci->adres}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputDogumYeri">Engel Kategorisi</label>
                                  <select class="select2" name="engel_kategorisi" style="width:100%;" id="inputEngel_kategorisi" >
                                    <option selected disabled>Kategori Seçin</option>
                                    <option value="Zihinsel Engelli" <?php
                                    if ($ogrenci->engel_kategorisi == 'Zihinsel Engelli') {
                                      echo 'selected';
                                    } ?>>Zihinsel Engelli</option>
                                    <option value="Bedensel Engelli" <?php
                                    if ($ogrenci->engel_kategorisi == 'Bedensel Engelli') {
                                      echo 'selected';
                                    } ?>>Bedensel Engelli</option>
                                  </select>
                              </div>
                          </div><!-- /.box-body -->
                          <div class="box-footer">
                              <button type="submit" class="btn btn-primary" onclick="saveStudent()">Kaydet</button>
                          </div>
                      </div>
                  </div><!-- /.box -->
              </div><!--/.col (left) -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()
