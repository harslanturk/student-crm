@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Profil
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Profil</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="img-responsive" src="
                          @if(isset($personelResim))
                          {{ $personelResim->resim }}
                          @else
                          /img/user.png
                          @endif" alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $person->name }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->

                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <form action="/admin/staff/update-resim" method="post" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="form-group">
                                          <label for="exampleInputFile"><b>Profil Resmi Değiştir:</b></label>
                                          <input type="file" id="profilResim" name="resim">
                                          <input type="hidden" value="{{ $person->id }}" name="personel_id" id="presim_personel_id">
                                      </div>
                                      <div>
                                          <button type="submit" class="btn btn-primary" >Kaydet</button><!--onclick="saveStudentImage()"-->
                                      </div>
                                  </form>
                              </li>
                              <li class="list-group-item">
                                  <b>Yetki:</b> <a class="pull-right">{{ $person->delegation->name }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>E-Mail:</b> <a class="pull-right">{{ $person->email }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>Telefon:</b> <a class="pull-right">{{ $person->telefon }}</a>
                              </li>
                          </ul>
                          <a href="/logout" class="btn btn-danger btn-block none-print"><b>Çıkış</b></a>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs">
                          <li class="active"><a href="#notification" data-toggle="tab">Bildirimler</a></li>
                          <li><a href="#settings" data-toggle="tab">Ayarlar</a></li>
                      </ul>
                      <div class="tab-content">
                          <div class="active tab-pane" id="notification">
                              <!-- The timeline -->
                              <ul class="timeline timeline-inverse">
                                  <!-- timeline time label -->
                                  <li class="time-label">
                                    <span class="bg-blue">
                                      Tüm Bildirimleriniz
                                    </span>
                                  </li>
                                  <!-- /.timeline-label -->
                                  @foreach($notification as $ntnvalue)
                                      <?php
                                      if($ntnvalue->type==1)
                                      {
                                          $bgclass = "bg-green";
                                      }
                                      elseif($ntnvalue->type==2)
                                      {
                                          $bgclass = "bg-aqua";
                                      }
                                      elseif($ntnvalue->type==3)
                                      {
                                          $bgclass = "bg-blue";
                                      }
                                      elseif($ntnvalue->type==4)
                                      {
                                          $bgclass = "bg-green";
                                      }
                                      elseif($ntnvalue->type==5)
                                      {
                                          $bgclass = "bg-aqua";
                                      }
                                      ?>
                                      <li id="profileNtfn{{ $ntnvalue->id }}">
                                          <i class="fa fa-asterisk {{ $bgclass }}"></i>
                                          <div class="timeline-item">
                                              <span style="color:white" class="time"><i class="fa fa-clock-o"></i> <?php $newdate = App\Helpers\Helper::DateTimeConverter($ntnvalue->created_at); echo $newdate; ?></span>
                                              <h3 class="timeline-header {{ $bgclass }}">{{ $ntnvalue->content }}</h3>
                                              <div class="timeline-body">
                                              </div>
                                              <div class="timeline-footer">
                                                  <a href="{{ $ntnvalue->url }}" class="btn btn-primary btn-xs">Bildirime Git</a>
                                                  <!--<button class="btn btn-danger btn-xs" onclick="ntfnDelete({{ $ntnvalue->id }});" data-widget="remove">Sil</button>-->
                                              </div>
                                          </div>
                                      </li>
                              @endforeach
                              <!-- END timeline item -->
                                  <li>
                                      <i class="fa fa-clock-o bg-gray"></i>
                                  </li>
                              </ul>
                          </div>
                          <!-- /.tab-pane -->

                          <div class="tab-pane" id="settings">
                              <form class="form-horizontal" action="/admin/staff/profile/save" method="post">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="user_id" value="{{ $person->id }}">
                                  <div class="form-group">
                                      <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>

                                      <div class="col-sm-10">
                                          <input type="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ $person->email }}" name="email">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputPassword" class="col-sm-2 control-label">Şifre</label>

                                      <div class="col-sm-10">
                                          <input type="password" class="form-control" id="inputPassword" placeholder="Şifre" name="password">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputName" class="col-sm-2 control-label">Ad Soyad</label>

                                      <div class="col-sm-10">
                                          <input type="text" class="form-control" id="inputName" placeholder="Ad Soyad" value="{{ $person->name }}" name="name">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputTCNo" class="col-sm-2 control-label">TC No</label>

                                      <div class="col-sm-10">
                                          <input type="text" class="form-control" id="inputTCNo" placeholder="TC No" value="{{ $person->tcno }}" name="tcno">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputCinsiyet" class="col-sm-2 control-label">Cinsiyet</label>

                                      <div class="col-sm-10">
                                          <select class="form-control" id="inputCinsiyet" name="cinsiyet">
                                          <option disabled>Seçiniz</option>
                                          <option>Bay</option>
                                          <option>Bayan</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputDogumTarihi">Doğum Tarihi</label>
                                      <div class="col-sm-10">
                                      <input type="date" class="form-control" id="inputDogumTarihi" name="dogum_tarihi" value="{{ $person->dogum_tarihi }}" placeholder="Doğum Tarihi">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputDogumYeri">Doğum Yeri</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputDogumYeri" name="dogum_yeri" value="{{ $person->dogum_tarihi }}" placeholder="Doğum Yeri">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputBabaAdi">Baba Adı Soyadı</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputBabaAdi" name="baba" value="{{ $person->baba }}" placeholder="Baba Adı Soyadı">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputAnneAdi">Anne Adı Soyadı</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputAnneAdi" name="anne" value="{{ $person->anne }}" placeholder="Anne Adı Soyadı">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputKayitTarihi">Kayıt Tarihi</label>
                                      <div class="col-sm-10">
                                      <input type="date" class="form-control" id="inputKayitTarihi" name="kayit_tarihi" value="{{ $person->kayit_tarihi }}" placeholder="Kayıt Tarihi">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputAyrilisTarihi">Ayrılış Tarihi</label>
                                      <div class="col-sm-10">
                                      <input type="date" class="form-control" id="inputAyrilisTarihi" name="ayrilis_tarihi" value="{{ $person->ayrilis_tarihi }}" placeholder="Ayrılış Tarihi">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputTelefon">Telefon</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputTelefon" name="telefon" value="{{ $person->telefon }}" placeholder="Diğer Telefon">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputIsTelefon">İş Telefon</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputIsTelefon" name="is_telefon" value="{{ $person->is_telefon }}" placeholder="İş Telefon">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputDigerTelefon">Diğer Telefon</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputDigerTelefon" name="diger_telefon" value="{{ $person->diger_telefon }}" placeholder="Diğer Telefon">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputIl">İl</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputIl" name="il" value="{{ $person->il }}" placeholder="İl">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputIlce">İlçe</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputIlce" name="ilce" value="{{ $person->ilce }}" placeholder="İlçe">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="inputAdres">Adres</label>
                                      <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputAdres" name="adres" value="{{ $person->adres }}" placeholder="Adres">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-10">
                                          <button type="submit" class="btn btn-danger">Ayarları Kaydet</button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <!-- /.tab-pane -->
                      </div>
                      <!-- /.tab-content -->
                  </div>
                  <!-- /.nav-tabs-custom -->
              </div>
              <!-- /.col -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()