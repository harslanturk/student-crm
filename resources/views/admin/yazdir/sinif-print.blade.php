<head>
  <title>Özel Siverek Şefkat Özel Eğitim Ve Rehabilitasyon Merkezi</title>
</head>
<style type="text/css">
  @@media print {
    #yazdir{
      display: none;
    }
  }
</style>
<div style="text-align:center">
  <h3>Sınıf Listesi</h3>
</div>
<button type="button" id="yazdir" onclick="window.print();">Yazdır</button>
    <table style="width:100%;text-align:center;" border="1">
    <tr>
      <td>#</td>
      <td style="width:19%;">Pazartesi</td>
      <td style="width:19%;">Salı</td>
      <td style="width:19%;">Çarşamba</td>
      <td style="width:19%;">Perşembe</td>
      <td style="width:19%;">Cuma</td>
    </tr>
    <?php $grup_no = 1;
    //$deger = "";
    // foreach($ogrencis as $key => $ogrenci){
    //   $deger .='<option value="'.$ogrenci->id.'">'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
    // }
     ?>
    @foreach($grups as $key => $grup)
    <tr style="background-color:lightgreen;width:100%;">
      <td colspan="6">{{$grup->name}}
        {{$grup_no}}.Seans
        {{$grup->seans_one_start}} -  {{$grup->seans_one_end}}  /  {{$grup->mola}} dk Mola
      <?php $grup_no++; ?>
        {{$grup_no}}.Seans
        {{$grup->seans_two_start}} - {{$grup->seans_two_end}}  /  {{$grup->mola}} dk Mola
      </td>
    </tr>
    <?php $grup_no++; ?>
    @foreach($sinifs as $anahtar => $sinif)
    <tr>
      <td>{{($anahtar+1)}}</td>
      <?php
        $ogrenci = App\Helpers\helper::SinifYazdir($sinif->name,'pazartesi',$id,($key+1));
        // echo '<pre>';
        // echo $sinif->name.'<br>';
        // echo $servis_id.'<br>';
        // echo $grup_no.'<br>';
        // echo ($key+1).'<br>';
        // die();
       ?>
      <td>
        @if($ogrenci)
        {{$sinif->name.' '.$ogrenci->ad.' '.$ogrenci->soyad}}
        @else
        &nbsp;
        @endif
      </td>
        <?php
          $ogrenci = App\Helpers\helper::SinifYazdir($sinif->name,'sali',$id,($key+1));
          // echo '<pre>';
          // print_r($ogrenci);
          // die();
         ?>
        <td>
          @if($ogrenci)
          {{$sinif->name.' '.$ogrenci->ad.' '.$ogrenci->soyad}}
          @else
          &nbsp;
          @endif
        </td>
        <?php
          $ogrenci = App\Helpers\helper::SinifYazdir($sinif->name,'carsamba',$id,($key+1));
          // echo '<pre>';
          // print_r($ogrenci);
          // die();
         ?>
        <td>
          @if($ogrenci)
          {{$sinif->name.' '.$ogrenci->ad.' '.$ogrenci->soyad}}
          @else
          &nbsp;
          @endif
        </td>
        <?php
          $ogrenci = App\Helpers\helper::SinifYazdir($sinif->name,'persembe',$id,($key+1));
          // echo '<pre>';
          // print_r($ogrenci);
          // die();
         ?>
        <td>
          @if($ogrenci)
          {{$sinif->name.' '.$ogrenci->ad.' '.$ogrenci->soyad}}
          @else
          &nbsp;
          @endif
        </td>
        <?php
          $ogrenci = App\Helpers\helper::SinifYazdir($sinif->name,'cuma',$id,($key+1));
          // echo '<pre>';
          // print_r($ogrenci);
          // die();
         ?>
        <td>
          @if($ogrenci)
          {{$sinif->name.' '.$ogrenci->ad.' '.$ogrenci->soyad}}
          @else
          &nbsp;
          @endif
        </td>
    </tr>
    @endforeach
    @endforeach
  </table>
