<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title>Özel Siverek Şefkat Özel Eğitim Ve Rehabilitasyon Merkezi</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.css">
    <link rel="stylesheet" href="/css/main.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/plugins/select2/select2.min.css">
    <!-- Sweet Alert -->
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/dist/sweetalert.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/plugins/dropzone/dropzone.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Slimscroll -->
    <script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>S</b>RHA</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Şefkat</b>RHA</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

            <!-- Notifications: style can be found in dropdown.less -->
                <?php
                $user = Auth::user();
                $allNotification = \App\Notification::where('target_id', $user->id)->where('status', '1')->orderBy('created_at', 'desc')->get();
                $allNotificationCount = \App\Notification::where('target_id', $user->id)->where('status', '1')->count();

                ?>
                <li class="dropdown notifications-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">{{ $allNotificationCount }}</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">{{ $allNotificationCount }} Yeni Bildiriminiz var</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    @foreach($allNotification as $notificationValue)
                      <li>
                        <a href="{{ $notificationValue->url }}">
                          <i class="fa fa-users text-aqua"></i> {{ $notificationValue->content }}
                        </a>
                      </li>
                    @endforeach
                    </ul>
                  </li>
                  <li class="footer"><a href="/admin/staff/profile/{{$user->id}}">Hepsini Göster</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <?php
                    /* <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>*/
              ?>

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="
                  <?php $presim = Auth::user(); $personelResim = App\PersonelResim::where('personel_id', $presim->id)->first(); ?>
                  @if(isset($personelResim))
                  {{ $personelResim->resim }}
                  @else
                          /img/user.png
                          @endif" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ $user=Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="@if(isset($personelResim))
                    {{ $personelResim->resim }}
                    @else
                            /img/user.png
                            @endif" class="img-circle" alt="User Image">
                    <p><?php $auser = Auth::user(); ?>
                      {{$user=Auth::user()->name}}
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/admin/staff/profile/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Profil</a>
                    </div>
                    <div class="pull-right">
                      <a href="/logout" class="btn btn-default btn-flat">Çıkış</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="@if(isset($personelResim))
              {{ $personelResim->resim }}
              @else
                      /img/user.png
                      @endif" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
            <div class="input-group">
              <input type="text" name="q" class="form-control notificationLink" placeholder="Öğrenci Ara" id="notificationLink" onkeyup="getSearch()">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat notificationLink"><i class="fa fa-search"></i></button>
              </span>
              <div id="notification_li">
                  <div id="notificationContainer" style="display: none;">
                      <div id="notificationTitle">Arama Sonuçları</div>
                      <div id="notificationsBody" class="notifications">
                          <div class="notificationsImg">
                          </div>
                          <div class="notificationsText">
                          Arama yapmak için en az 4 harf giriniz.
                          </div>
                      </div>
                      <!--<div id="notificationFooter" onclick="window.location='/ara/';" style="cursor:pointer;">Tümünü Göster</div>-->
                  </div>
              </div>
            </div>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENÜ</li>
            <?php
              $auths = App\Helpers\helper::getAuthParent(Auth::user()->id);
              $subParent = App\Helpers\helper::getAuthSubParent(Auth::user()->id);
              //print_r($auths);
            ?>
            @foreach($auths as $auth)
              <?php

              if($auth->read==1){
                if($auth->parent==1){
                  echo ' <li class="treeview">
                <a href="#">
                  <i class="fa '.$auth->icon.'"></i>
                  <span>'.$auth->name.'</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                ';
                  foreach ($subParent as $val) {
                    if($val->read==1 && $val->parent_id==$auth->id){
                      echo '<li><a href="'.$val->url.'"><i class="fa '.$val->icon.'"></i> '.$val->name.'</a></li>';
                    }
                  }
                  echo '
                </ul>
              </li>';
                }else{
                  echo '<li class="treeview">
                  <a href="'.$auth->url.'">
                    <i class="fa '.$auth->icon.'"></i> <span>'.$auth->name.'</span> </a>

              </li>';}

              }
              ?>

            @endforeach
            <!--<li class="active treeview">
              <a href="/">
                <i class="fa fa-dashboard"></i> <span>Anasayfa</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Öğrenci</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/tum-ogrenciler"><i class="fa fa-circle-o"></i> Tüm Öğrenciler</a></li>
                <li><a href="/ogrenci-ekle"><i class="fa fa-circle-o"></i> Öğrenci Ekle</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Personel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/tum-personeller"><i class="fa fa-users"></i> Tüm Personeller</a></li>
                <li><a href="/personel-ekle"><i class="fa fa-user-plus"></i> Personel Ekle</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-info"></i>
                <span>Tanımlamalar</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/ogrenci-islemleri"><i class="fa fa-info"></i> Öğrenci İşlemleri</a></li>
                <li><a href="/engel-tipleri"><i class="fa fa-user-plus"></i> Engel Tipleri</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-plus"></i>
                <span>Yetkilendirme İşlemleri</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-comment-o"></i>
                <span>Mesajlar</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-comment-o"></i> Gelen Kutusu</a></li>
                <li><a href="#"><i class="fa fa-comment-o"></i> Giden Kutusu</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-comment-o"></i>
                <span>SMS Modülü</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-comment-o"></i> Hızlı SMS Gönder</a></li>
                <li><a href="#"><i class="fa fa-comment-o"></i> SMS Şablonları</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-question"></i>
                <span>Danışman Modülü</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-question"></i>
                <span>Destek</span>
              </a>
            </li>-->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      @yield('content')
      <footer class="main-footer none-print">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.2
        </div>
        <strong>Copyright &copy; 2016 <a href="http://cozumlazim.com">ÇözümLazım.com</a>.</strong> Tüm Hakları Saklıdır.
      </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- Modal Alanı -->

    <div class="modal modal-success fade" id="modalIlkMesaj" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form role="form">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Bilgilendirme</h4>
          </div>
          <div class="modal-body">
              <div class="box-body">
                1-	Öğrenci işlemleri (ekleme, arama, silme, listeleme, misafir öğrenci) --- Tamamlandı<br>
                2-	Personel İşlemleri (ekleme, arama, silme, listeleme)---- Tamamlandı<br>
                3-	Personeller arası mesajlaşma ---- Tamamlandı Fakat whatsapp şeklinde tekrar yapılıyor. Testleri bittikten sonra yayınlanacak<br>
                4-	SMS Modülü (Kontür satın alınması gerekecek. Kontür fiyatları ayrıca bildirelecektir.) --- Ekranları hazır fakat sms firmasının api göndermesi bekleniyor<br>
                5-	Personel Yetkilendirme Sistemi (yetkileri ayrıca gruplandırma)--- Ekranı hazır  fakat yayınlanması için diğer sayfaların bitmesi gerekiyor.<br>
                6-	Danışman Bilgi Modülü--- Testleri yapılıyor<br>
                7-	Öğrenci İşlem Modülü --- Testleri yapılıyor<br>
                8-	Muhasebe Modülü--- İşlem modülleri bittikten sonra danışman firmayla görüşülüp isteklerine bağlı olarak düzenlenecek.<br>
                9-	Öğrenci Planlama Modülü--- Tasarımı yapılıyor.<br>

              </div><!-- /.box-body -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- /Modal Alanı -->
    <!-- fullCalendar 2.2.5 -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/lang-all.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="/plugins/select2/select2.full.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- DataTables -->
    <script src="/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- InputMask -->
    <script src="/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- bootstrap color picker -->
    <script src="/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- FastClick -->
    <script src="/plugins/fastclick/fastclick.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="/plugins/chartjs/Chart.min.js"></script>
    <!--AJAX-->
    <script src="/js/ajax.js"></script>
    <script src="/js/custom.js"></script>
    <!-- AdminLTE App -->
    <script src="/js/app.js"></script>
    <!-- Sweet Alert -->
    <script src="/plugins/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Dropzone -->
    <script src="/plugins/dropzone/dropzone.js"></script>
    <script>
        if(window.location.pathname.split('/')[2] == 'sms'){
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_secerek').value.length;
            document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            var kalan_karakter_2 = 160 - document.getElementById('sms_mesaj_elle').value.length;
            document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter_2;
            var kalan_karakter_1 = 160 - document.getElementById('sms_mesaj_personel').value.length;
            document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter_1;
        }
        function islem_gonder_elle() {
            var kalan_karakter = 160 - document.getElementById('islem_mesaj_secerek').value.length;
            document.getElementById('islem_kalan_karakter').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('islem_kalan_karakter').innerHTML = kalan_karakter;
            }else{
                document.getElementById('islem_mesaj_secerek').value = document.getElementById('islem_mesaj_secerek').value.substring(0,160);
                console.log('substr')
                document.getElementById('islem_kalan_karakter').innerHTML = kalan_karakter;
            }
        }
        function sms_gonder_elle() {
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_secerek').value.length;
            document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            }else{
                document.getElementById('sms_mesaj_secerek').value = document.getElementById('sms_mesaj_secerek').value.substring(0,160);
                console.log('substr')
                document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            }
        }
        function sms_gonder_elle_2() {
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_elle').value.length;
            document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter;
            }else{
                document.getElementById('sms_mesaj_elle').value = document.getElementById('sms_mesaj_elle').value.substring(0,160);
                console.log('substr')
                document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter;
            }
        }function sms_gonder_elle_3() {
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_personel').value.length;
            document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter;
            }else{
                document.getElementById('sms_mesaj_personel').value = document.getElementById('sms_mesaj_personel').value.substring(0,160);
                console.log('substr')
                document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter;
            }
        }
      /*$(window).load(function(){
        $('#modalIlkMesaj').modal('show');
      });*/
      $("#calendar").datepicker({
        language: 'tr'
      });
        //Colorpicker
        $(".my-colorpicker1").colorpicker();

      $("[data-mask]").inputmask();
        function addStudent()
         {
            var inputOgrenciNo = document.getElementById('inputOgrenciNo').value;
            var inputTcNo = document.getElementById('inputTcNo').value;
            var inputEngelTipi = document.getElementById('inputEngelTipi').value;
            var inputAdi = document.getElementById('inputAdi').value;
            var inputSoyadi = document.getElementById('inputSoyadi').value;
            var inputCinsiyeti = document.getElementById('inputCinsiyeti').value;
            var inputDogumTarihi = document.getElementById('inputDogumTarihi').value;
            var inputDogumYeri = document.getElementById('inputDogumYeri').value;
            var inputOkulunAdi = document.getElementById('inputOkulunAdi').value;
            var inputBabaAdi = document.getElementById('inputBabaAdi').value;
            var inputBabaTel = document.getElementById('inputBabaTel').value;
            var inputKayitTarihi = document.getElementById('inputKayitTarihi').value;
            var inputAnneAdi = document.getElementById('inputAnneAdi').value;
            var inputAnneTel = document.getElementById('inputAnneTel').value;
            var inputAyrilisTarihi = document.getElementById('inputAyrilisTarihi').value;
            var inputEgitimSekli = document.getElementById('inputEgitimSekli').value;
            var inputDigerTel = document.getElementById('inputDigerTel').value;
            var inputIl = document.getElementById('inputIl').value;
            var inputIlce = document.getElementById('inputIlce').value;
            var inputAdres = document.getElementById('inputAdres').value;
            $.ajax({
              url: '/admin/students/new',
              type: 'POST',
              beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
              cache: false,
              data: {inputOgrenciNo: inputOgrenciNo, inputTcNo:  inputTcNo, inputEngelTipi:  inputEngelTipi, inputAdi:  inputAdi, inputSoyadi:  inputSoyadi, inputCinsiyeti:  inputCinsiyeti, inputDogumTarihi:  inputDogumTarihi, inputDogumYeri:  inputDogumYeri, inputOkulunAdi:  inputOkulunAdi, inputBabaAdi:  inputBabaAdi, inputBabaTel:  inputBabaTel, inputKayitTarihi:  inputKayitTarihi, inputAnneAdi:  inputAnneAdi, inputAnneTel:  inputAnneTel, inputAyrilisTarihi:  inputAyrilisTarihi, inputEgitimSekli:  inputEgitimSekli, inputDigerTel:  inputDigerTel, inputIl:  inputIl, inputIlce:  inputIlce, inputAdres:  inputAdres },
              success: function(data){
              location.href = "/";
              alert("Öğrenci Başarıyla Eklendi");
              },
              error: function(jqXHR, textStatus, err){}
            });
         }
      function saveStudentImage()
      {
        var inputPresim = document.getElementById('profilResim').value;
        var inputPresimOgrenciId = document.getElementById('presim_ogrenci_id').value;
        $.ajax({
          url: '/admin/students/update-resim',
          type: 'POST',
          beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
          },
          cache: false,
          data: {ogrenci_id: inputPresimOgrenciId, resim: inputPresim },
          success: function(data){
            /*location.href = "/";
            location.reload();
            alert("Öğrenci Resim Başarıyla Eklendi");*/
          },
          error: function(jqXHR, textStatus, err){}
        });
      }
        function saveStudent()
         {
            var inputOgrenciId = document.getElementById('inputOgrenciId').value;
            var inputOgrenciNo = document.getElementById('inputOgrenciNo').value;
            var inputTcNo = document.getElementById('inputTcNo').value;
            var inputEngelTipi = document.getElementById('inputEngelTipi').value;
            var inputAdi = document.getElementById('inputAdi').value;
            var inputSoyadi = document.getElementById('inputSoyadi').value;
            var inputCinsiyeti = document.getElementById('inputCinsiyeti').value;
            var inputDogumTarihi = document.getElementById('inputDogumTarihi').value;
            var inputDogumYeri = document.getElementById('inputDogumYeri').value;
            var inputOkulunAdi = document.getElementById('inputOkulunAdi').value;
            var inputBabaAdi = document.getElementById('inputBabaAdi').value;
            var inputBabaTel = document.getElementById('inputBabaTel').value;
            var inputKayitTarihi = document.getElementById('inputKayitTarihi').value;
            var inputAnneAdi = document.getElementById('inputAnneAdi').value;
            var inputAnneTel = document.getElementById('inputAnneTel').value;
            var inputAyrilisTarihi = document.getElementById('inputAyrilisTarihi').value;
            var inputEgitimSekli = document.getElementById('inputEgitimSekli').value;
            var inputDigerTel = document.getElementById('inputDigerTel').value;
            var inputIl = document.getElementById('inputIl').value;
            var inputIlce = document.getElementById('inputIlce').value;
            var inputAdres = document.getElementById('inputAdres').value;
            var inputEngel_kategorisi = document.getElementById('inputEngel_kategorisi').value;
            $.ajax({
              url: '/admin/students/update',
              type: 'POST',
              beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
              cache: false,
              data: {id: inputOgrenciId, ogrenci_no: inputOgrenciNo, tcno:  inputTcNo, engel_tipi:  inputEngelTipi, ad:  inputAdi, soyad:  inputSoyadi, cinsiyet:  inputCinsiyeti, dogum_tarihi:  inputDogumTarihi, dogum_yeri:  inputDogumYeri, okul:  inputOkulunAdi, baba:  inputBabaAdi, baba_tel:  inputBabaTel, kayit_tarihi:  inputKayitTarihi, anne:  inputAnneAdi, anne_tel:  inputAnneTel, ayrilis_tarihi:  inputAyrilisTarihi, egitim:  inputEgitimSekli, diger_tel:  inputDigerTel, il:  inputIl, ilce:  inputIlce, adres:  inputAdres, engel_kategorisi: inputEngel_kategorisi },
              success: function(data){
              /*location.href = "/";*/
                alert("Öğrenci Başarıyla Eklendi");
                location.reload();
              },
              error: function(jqXHR, textStatus, err){}
            });
         }
        function addPersonel()
         {
            var inputYetkiGrubu = document.getElementById('inputYetkiGrubu').value;
            var inputTcNo = document.getElementById('inputTcNo').value;
            var inputName = document.getElementById('inputName').value;
            var inputCinsiyeti = document.getElementById('inputCinsiyeti').value;
            var inputDogumTarihi = document.getElementById('inputDogumTarihi').value;
            var inputDogumYeri = document.getElementById('inputDogumYeri').value;
            var inputBabaAdi = document.getElementById('inputBabaAdi').value;
            var inputKayitTarihi = document.getElementById('inputKayitTarihi').value;
            var inputAnneAdi = document.getElementById('inputAnneAdi').value;
            var inputAyrilisTarihi = document.getElementById('inputAyrilisTarihi').value;
            var inputTelefon = document.getElementById('inputTelefon').value;
            var inputIsTelefon = document.getElementById('inputIsTelefon').value;
            var inputDigerTelefon = document.getElementById('inputDigerTelefon').value;
            var inputIl = document.getElementById('inputIl').value;
            var inputIlce = document.getElementById('inputIlce').value;
            var inputAdres = document.getElementById('inputAdres').value;
            var inputEmail = document.getElementById('inputEmail').value;
            var inputPass = document.getElementById('inputPass').value;
            var inputColor = document.getElementById('inputColor').value;
            var sinif_id = document.getElementById('sinif_id').value;
            var servis_id = document.getElementById('servis_id').value;
            $.ajax({
              url: '/admin/staff/new',
              type: 'POST',
              beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
              cache: false,
              data: {password: inputPass,email: inputEmail,delegation_id: inputYetkiGrubu, tcno:  inputTcNo, name:  inputName, cinsiyet:  inputCinsiyeti, dogum_tarihi:  inputDogumTarihi, dogum_yeri:  inputDogumYeri, baba:  inputBabaAdi, kayit_tarihi:  inputKayitTarihi, anne:  inputAnneAdi, ayrilis_tarihi:  inputAyrilisTarihi, telefon:  inputTelefon, is_telefon:  inputIsTelefon, diger_telefon:  inputDigerTelefon, il:  inputIl, ilce:  inputIlce, adres:  inputAdres,color: inputColor,sinif_id:sinif_id,servis_id:servis_id },
              success: function(data){
              location.href = "/";
              alert("Personel Ekleme Başarılı");
              },
              error: function(jqXHR, textStatus, err){}
            });
         }
        $(function () {
          $('#ogrenci-table,#authorization-table,#modulesTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true
          });
        });
      $(function () {
        //Add text editor
        $("#compose-textarea").wysihtml5();
      });
      $(".select2").select2({
        autocomplete: true
      });
      function silOnayla()
      {
        return confirm("Silmek istediğinizden emin misiniz?");
      }
      function deleteApprove(link) {
        swal({
                  title: "Silmek istediğinize emin misiniz?",
                  text: "Eğer silerseniz, sildiğiniz veriye bir daha ulaşamayacağınızı onaylamış olursunuz!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, veriyi sil!",
                  cancelButtonText: "Hayır, veriyi silme!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Silindi!", "Veriniz başarılı bir şekilde silindi.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "Verinize herhangi bir işlem yapılmadı.", "error");
                  }
                });
      }
      function deleteApproveStudent(link,konu) {
        if(konu > 0){
        swal({
                  title: "Öğrencinin Bitmemiş Konusu Bulunmakta Yinede Silmek İstiyormusunuz!",
                  text: "Eğer silerseniz, sildiğiniz veriye bir daha ulaşamayacağınızı onaylamış olursunuz!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, veriyi sil!",
                  cancelButtonText: "Hayır, veriyi silme!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Silindi!", "Veriniz başarılı bir şekilde silindi.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "Verinize herhangi bir işlem yapılmadı.", "error");
                  }
                });
                }
        else{
          swal({
                    title: "Silmek istediğinize emin misiniz?",
                    text: "Eğer silerseniz, sildiğiniz veriye bir daha ulaşamayacağınızı onaylamış olursunuz!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Evet, veriyi sil!",
                    cancelButtonText: "Hayır, veriyi silme!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                  },
                  function(isConfirm){
                    if (isConfirm) {
                      swal("Silindi!", "Veriniz başarılı bir şekilde silindi.", "success");
                      location.href = link;
                    } else {
                      swal("İptal Edildi", "Verinize herhangi bir işlem yapılmadı.", "error");
                    }
                  });
        }
      }
      function readApprove(link) {
        swal({
                  title: "Okundu durumunu değiştirmek istediğinize emin misiniz?",
                  text: "Eğer devam ederseniz, okundu / okunmadı olarak değiştirilecektir.",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, devam et!",
                  cancelButtonText: "Hayır, iptal!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Değiştirildi!", "Danışman notu başarılı bir şekilde değiştirildi.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "İşleminiz iptal edildi.", "error");
                  }
                });
      }
      function konuOpenApprove(link) {
        swal({
                  title: "Bu konuyu açmak istediğinize emin misiniz?",
                  text: "Eğer bu konuyu açarsanız, diğer kullanıcılar yeni işlem ekleyebilirler!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, bu konuyu aç!",
                  cancelButtonText: "Hayır, bu konuyu açma!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Konu Açıldı!", "Konunuz başarılı bir şekilde açıldı.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "Konunuz açılmamıştır.", "error");
                  }
                });
      }
      function getSearch()
       {
          var query = document.getElementById('notificationLink').value;
          var say=query.length;
          if(say>"1")
          {
          $.ajax({
            url: '/admin/students/search',
            type: 'POST',
            beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
            cache: false,
            data: {query: query},
            success: function(data){
              document.getElementById('notificationsBody').innerHTML=data;
            },
            error: function(jqXHR, textStatus, err){}
         });
          }
       }
       /* Notifikasyon Ekranı */
        $(".notificationLink").click(function()
        {
       //okundu işlemi burda yapılabilir.
            $("#notificationContainer").fadeToggle(300);
            $("#notification_count").fadeOut("slow");
            return false;
        });
     //Document Click
        $(document).click(function()
        {
            $("#notificationContainer").hide();
        });
      //Popup Click
        $("#notificationContainer").click(function()
        {
            //return false
        });

        /* Notifikasyon Ekranı */
    </script>
    @yield('edit-js')
  </body>
</html>
