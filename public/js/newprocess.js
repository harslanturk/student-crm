$(document).ready(function(e) {
    $("#personelCek").bind('change',islemleriGetir);
});


function islemleriGetir(){
	if($("#personelCek").val() != 0){
		$.ajax({
			url: '/admin/process/islemcek',
			type: 'POST',
			beforeSend: function (xhr) {
				var token = $('meta[name="csrf_token"]').attr('content');

				if (token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				}
			},
			cache: false,
			data: {aliciId: $("#personelCek").val() },
			success: function(data){
				$("#islemCek option").remove();
				$("#islemCek").append(data);
				console.log(data);
			},
			error: function(jqXHR, textStatus, err){}
		});
		/*$.post('/admin/muhasebe/destinasyonlaricek',{destinasyonId: $("#e3").val()},function(output){
			$("#e4 option").remove();
			$("#e4").append(output);
		});*/
	}
	else{
		$("#islemCek option").remove();
		$("#islemCek").append('<option value="0">Önce Personel Seçiniz</option>');
	}
}